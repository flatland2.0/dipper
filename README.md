# Scripts/Librairies de traitement des enregistrements d'oiseaux

Python translation of the R scripts used for bird song analysis

## Description

+ 2 libraries:
  * LenaWave:  adaptation of the seewave package of Jerome Sueur
  * LenaFunc: collection of usefull procedures
+ collection of scripts used by the main program Dipper_song_ampl.py
+ buildlib.py and buildlib.yml: utility (re)building the libraries, and installing them
+ test.py: script used to call all functions and verify them


## Requirements

The minimum version of python is 3.10 
The following libraries are used:
+ setuptools>=61.0 -> used by buildlib.py 
+ wave -> to read the recordings 
+ sounddevice -> used to play the waves through the PC speakers
+ matplotlib -> for oscillograms and spetrograms
+ numpy -> all wave info is stored as numpy arrays
+ pandas -> stores all extracted information and read/save as csv files
  
In order to install these: 

```
pip install setuptools wave sounddevice matplotlib numpy pandas
```

