#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Common procedures to treat CITs"
__contact__ = "framondj@gmail.com"


import math

# library(dplyr)
# library(reshape2)
# pip install pandas
import pandas as pd

import call_analysis

# library(ggplot2)
# pip install ggplot
import ggplot
import matplotlib as plt
import math

# library(seewave)
# library(tuneR)
import wave
import r_func
### SETTINGS  ###########################################################################
# sample rate
f=44100

# buffer time before and after a call was detected (in SAMPLES)
buf = 2250

# spectrogram window size (SAMPLES)
ws = 256

# smoothing of envelope (in SECONDS)
dur_smooth = 0.005
sm = f*dur_smooth

# dB threshold to define call duration and frequency max and min
ana_thr = [-12,-6]

# list of logs
logcoldBs = [math.log(x)*15-math.log(20)*15 for x in range(1,20)]

i=1

def call_ampl(session, meta):
    filename = f"../call_analysis/to_check_{session}.csv"
    all_call_pars = pd.read_csv(filename)

    fintab=pd.DataFrame()
    
    for m in meta:
        if m['Session'] != session:
            continue
        
        call_params = all_call_pars[all_call_pars['recnb'] == m['Recording_nb']]
        # read calibration file (just first three seconds after calib started)
        calib_name = f"./WAV/{m['Session']}/{m['Calibration_file']}.WAV"  
        
        # select just this part
        _, calib = call_analysis.readWave(calib_name, from_=m['Sec_start_calib'], to_=m['Sec_start_calib']+3, units="seconds")

        par(mfrow=c(1,1))
        spectro(calib, fastdisp = True)
        transfer = call_analysis.make_transfer(calib, f_tone=5000, RL_dBA= m['Ampl_calib_dBA'], f=f)
  
        names = []
        for j in range(1, len(ana_thr)):
            varname = f"rms_dB_FS_{-1*ana_thr[j]}dB"
            varname3 = f"pe_dB_FS_{-1*ana_thr[j]}dB"
            newcol = f"rms_dB_RL_{-1*ana_thr[j]}dB"
            newcol2 = f"rms_dB_SL_{-1*ana_thr[j]}dB"
            newcol3 = f"pe_dB_RL_{-1*ana_thr[j]}dB"
            newcol4 = f"pe_dB_SL_{-1*ana_thr[j]}dB"
            name = pd.data.frame(varname = rep([varname, varname3], each = 2), new = [newcol, newcol2, newcol3, newcol4])
            names.append(name)

        ampls = pd.DataFrame()
        for k in range(1,len(names['new']),2):
            RL = call_params[,names[varname[k]]] + transfer
            SL = call_params[,names[varname[k]]] + transfer + call_analysis.dB(m['Dist_bird_m'], ref = 1)
    
            st = pd.concat([pd.data.frame(RL, SL), ampls], ignore_index=True)
  
        ampltab = do.call(cbind, ampls)
    
          
        colnames(ampltab) = names['new']
  
        call_params.extend(ampltab)
  
        call_params['dist_bird_m'] = m['Dist_bird_m']
        call_params['amb_dBA'] = m['Ampl_noise_mean']
  
        call_params['ICI'] = None
        for k in range(2, len(call_params['dB_thr_6dB'])):
            call_params['ICI'][k] = call_params['starts_spl'][k]/f - call_params['ends_spl'][k-1]/f

        st = pd.concat([pd.DataFrame(call_params, columns=call_params.columns), call_params], ignore_index=True)


    fintab = fintab[-fintab$exclude,]
    
    ana = []
    for i in range(1,15):
        for a in ana_thr:
            ana.append(-1*a)
    col_order = [
        "sess", 
        "recnb", 
        "dist_bird_m", 
        "amb_dBA", 
        "filt", 
        "add_filt", 
        "call_time_6dB", 
        "ICI", 
        "call_dur", 
        "Fp_kHz",
        "loF_kHz",
        "hiF_kHz",
        "bw",
        "Fstart_kHz",
        "Fend_kHz",
        "Fmax_ampl_kHz",
        "pe_dB_FS",
        "rms_dB_FS",
        "pe_dB_RL",
        "rms_dB_RL",
        "pe_dB_SL",
        "rms_dB_SL",
        "SNR"
        "_"
        ]
    col_order += ana + ["dB"] 
    
    fintab = fintab[,col_order]


    fintab=pd.DataFrame(columns=col_order)

    # save results
    filename = f"../data/final_{session}.csv"
    if fintab.to_csv(filename, encoding='utf-8', index=False, header=False):
        print("file saved! Your analysis is finished!")