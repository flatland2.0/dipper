#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "2.1.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Procedure to build and install libraries"
__contact__ = "framondj@gmail.com"

######################################################################################
# 1/ Use the yaml file buildlib.yml to get the list of libraries to (re)build
#    the parameter -l allows to select on or more libraries (separate names by a colon (,))
#    the parameter -n display the list of known libraries
# 2/ Create the toml file used by setuptools
# 3/ Spawn the build process
# 4/ Spawn the pip installation of the new library
import argparse
import codecs
import os
import subprocess
import sys
import regex
import yaml
import logging


def last(ldir, pkg):
    """
    parse the list of versions, and return only the last one
    Args:
        ldir (string): directory to parse
        pkg (string): name of the library
    Returns:
        string: file name
    """
    v0 = 0
    lf = ""
    for file in os.listdir(f"{ldir}"):
        f = file
        r = rf"{pkg}-(\d+).(\d+).(\d+).py3-none-any.whl"
        m = regex.search(r, f, regex.IGNORECASE)
        if m:
            v = int(m.group(3)) + 1000 * int(m.group(2)) + 10000 * int(m.group(1))
            if v > v0:
                v0 = v
                lf = file
        else:
            continue
    return lf


def get_data(path):
    """
    Open the library source file, and search for all __xxx__ variables used to describe the package
    Args:
        path (string): file name
    Raises:
        RuntimeError: Stop if no version strings
    Returns:
        dict: all needed info
    """
    data = {"version": None, "author": None, "description": None, "license": None, "copyright": None, "contact": None}
    with codecs.open(path, "r") as fp:
        r = r"__([^\_]+)__ = [\"|\']([^\"\']+)[\"|\']"
        for line in fp.read().splitlines():
            if m := regex.search(r, line, regex.IGNORECASE):
                if m.group(1) not in ["author", "copyright", "license", "version", "description", "contact"]:
                    continue
                data[m.group(1)] = m.group(2)
        if (
            not data["version"]
            or not data["author"]
            or not data["description"]
            or not data["license"]
            or not data["copyright"]
            or not data["contact"]
        ):
            raise RuntimeError("unable to find version strings")
        return data


def build_toml(d, libname):
    """
    Create the file used to build the package
    Args:
        d (string): path to the package
        libname: package name
    """
    data = get_data(f"{d}/src/{libname}/{libname}.py")
    authors = [{"name": a, "email": c} for a, c in zip(data["author"].split("/"), data["contact"].split(","))]

    txt = f"""[build-system]
requires = ["setuptools>=61.0", "wave","matplotlib", "numpy", "pandas", "sounddevice"]
build-backend = "setuptools.build_meta"

[project]
name = "{libname}"
version = "{data["version"]}"
authors = [
"""
    for a in authors:
        txt += f"""\t{{name="{a['name']}", email="{a['email']}"}},\n"""
    txt += f"""\t]
description = "{data["description"]}"
readme = "README.md"
requires-python = ">=3.10"
classifiers = [
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: {data["license"]} License",
    "Operating System :: OS Independent",
]

    [project.urls]
    "Homepage" = "https://gitlab.com/flatland2.0/dipper"
    "Bug Tracker" = "https://gitlab.com/flatland2.0/dipper/issues"
"""
    with open(f"{d}/pyproject.toml", "w") as f:
        f.write(txt)


def main():
    try:
        prog = os.path.splitext(os.path.basename(sys.argv[0]))[0]
        parser = argparse.ArgumentParser(description="update git repos", usage="%(prog)s [options] \n %(prog)s -h for help\n\n")
        parser.add_argument("-l", "--libr", action="store", default=None, help="librares to update")
        parser.add_argument("-n", "--names", action="store_true", default=False, help="List of current libraries")
        parser.add_argument("-v", "--verbose", action="store_true", default=False, help="debug flag")
        options = parser.parse_args()
        lgr = logging.getLogger(name="buildLib")
        logging.basicConfig(
            level=logging.DEBUG if options.verbose else logging.INFO,
            force=True,
            format="%(msg)s",
        )
        # get list of libraries
        with open(f"{os.path.dirname(os.path.realpath(__file__))}/buildlib.yml") as f:
            libraries = yaml.load(f, Loader=yaml.FullLoader)["libraries"]
        # list them and quit if -n on command line
        if options.names:
            for lb in libraries:
                sep = "\t\t" if len(lb["name"]) > 9 else "\t\t\t"
                print(f"name: {lb['name']}{sep}dir: {lb['dir']}  ")
            sys.exit(0)

        # get selected libraries from command line (defaults to all)
        selected_libs = [l["name"] for l in libraries]
        if options.libr:
            if options.libr.casefold() != "all":
                selected_libs = options.libr.split(",")

        # loop around the list, keeping only the selected ones
        for lib in libraries:
            if next((l for l in selected_libs if l == lib["name"]), None):
                lgr.info(f"**** {lib['name']} {'*'*(75-len(lib['name']))}")
                ldir = f"{'D:' if os.name == 'nt' else ''}{lib['dir']}"
                lgr.info(f"++++ build toml file {'+'*60}")
                build_toml(ldir, libname=lib["name"])
                lgr.info(f"++++ build wheel {'+'*64}")
                subprocess.run(["python", "-m", "build", "-n"], cwd=lib["dir"], check=False)
                newlib = last(f"{ldir}/dist", lib["name"])
                pkg = f"{ldir}/dist/{newlib}"
                lgr.debug(f"{newlib} - {pkg}")
                lgr.info(f"++++ install new library {'+'*56}")
                subprocess.run(["python", "-m", "pip", "install", "--upgrade", "--force-reinstall", pkg], check=False)

        lgr.info(f"--- End of {prog} {'-'*(69-len(prog))}")

    except Exception as msg:
        print(msg)


# MAIN ########################################################################
if __name__ == "__main__":
    main()
    sys.exit(0)
