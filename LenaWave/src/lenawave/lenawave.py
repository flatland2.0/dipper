#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Lena de Framond/Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "python implementation of a seeWave like library"
__contact__ = "lena.de.framond@laposte.net,framondj@gmail.com"

##########################################################################
# Attempt to build a python library behaving as much as possible as the R seeWave package
# the algorithms used are a direct translation of R
# As we do not use stereo sound, we use 1D arrays to simplify.
# Acknoledgement:
# Jerome Sueur: sueur@mnhn.fr
# the seewave package can be found at https://cran.r-project.org/web/packages/seewave/


import math
import wave
import matplotlib.pyplot as plt
import numpy as np
import sounddevice as sd
from enum import Enum


class Wave:
    def __init__(self, sound=None, rate=44100, bit=16, pcm=True):
        self.rate: int = rate
        self.bit: int = bit
        self.pcm: bool = pcm
        self.sound: np.array = None
        self.set(sound=sound)

    class UNITS(Enum):
        hours = 3600
        minutes = 60
        seconds = 1

    def __enter__(self):
        return self

    def __del__(self):
        pass

    def __exit__(self, *args):
        pass

    def setValidity(self):
        if not isinstance(self.rate, int):
            raise ValueError("'rate' must be a positive integer")
        if self.bit not in [1, 8, 16, 24, 32, 64]:
            return "'bit' must one of (1, 8, 16, 24, 32 or 64)"
        if not isinstance(self.pcm, bool):
            return "'pcm' must be True or False"
        if self.pcm and self.bit == 64:
            raise ValueError("pcm Waves must have a resolution < 64 bit")
        if not self.pcm and self.bit not in [32, 64]:
            raise ValueError("Float (non pcm) Waves must have a resolution of either 32 or 64 bit")
        return True

    ##### Generate Wave objects ################################################################
    def newWave(self, sound=None):
        """
        generate a modified copy of a Wave with supplied sound. defaults to empty sound
        Args:
            sound (np.array, optional): sound. Defaults to None.
        Returns:
            Wave
        """
        w = Wave(bit=self.bit, rate=self.rate, pcm=self.pcm)
        w.set(sound=sound)
        return w

    def set(self, sound=None):
        """
        Inplace set sound, and compute frames and duration
        Args:
            sound (np.array, optional): sound. Defaults to None.
        """
        self.sound = np.zeros((1,), dtype=float) if sound is None else sound
        self.frames: int = len(self.sound)
        self.duration: float = self.frames / self.rate

    def read(rec_name, from_=None, to_=None, units="seconds", plot=False, listen=False):
        """
        simulate the behavior of the tuneR readWave function
        Args:
            rec_name: wave file name
            from_: starting time. Defaults to None.
            to_: ending time. Defaults to None.
            units: one of seconds, minutes, hours. Defaults to "seconds".
            plot (bool, optional): _description_. Defaults to False.
            listen (bool, optional): _description_. Defaults to False.
        Returns:
            Wave soundclass
        """
        u = Wave.UNITS[units].value
        with wave.open(rec_name, "rb") as wav:
            params = wav.getparams()
            w = Wave()
            nframes = params.nframes / params.nchannels
            from_frame = round(from_ * u * params.framerate) if from_ else 0
            to_frame = round(to_ * u * params.framerate) if to_ else nframes
            if from_frame >= nframes:
                from_frame = 0
            if to_frame > nframes:
                to_frame = nframes
            frames = to_frame - from_frame
            wav.setpos(from_frame)
            wavFrames = wav.readframes(frames)
            signal_array = np.array(np.frombuffer(wavFrames, dtype=np.int16), dtype=float)
            w.set(sound=signal_array[0::2] if params.nchannels == 2 else signal_array)

        Wave.display(w, plot=plot, listen=listen)
        return w

    def synth(f, d, rate=44100, units="seconds", plot=False, listen=False):
        """
        Generate a Wave object of a given frequency and duration
        Args:
            f (float, optional): frequency of the beep in Hz. Defaults to 1000.0.
            d (float, optional): duration of the beep in seconds. Defaults to 1.0.
            rate (int, optional): sample rate. Defaults to 44100.
            plot (bool, optional): _description_. Defaults to False.
            listen (bool, optional): _description_. Defaults to False.
        Returns:
            Wave object
        """
        t = np.array([math.sin(f * x) for x in np.linspace(0, d * 2 * math.pi, num=round(rate * d * Wave.UNITS[units].value))])
        m = max(abs(t))
        t = np.array([(x / m) for x in t])
        w = Wave(sound=t)
        Wave.display(w, plot=plot, listen=listen)
        return w

    def noise(d, type="unif", rate=44100, units="seconds", plot=False, listen=False):
        """
        Create waave with random noise
        Args:
            d (_type_): _description_
            type (str, optional): _description_. Defaults to "unif".
            rate (int, optional): _description_. Defaults to 44100.
            units (str, optional): _description_. Defaults to "seconds".
            plot (bool, optional): _description_. Defaults to False.
            listen (bool, optional): _description_. Defaults to False.
        Returns:
            Wave object
        """
        frames = round(rate * d * Wave.UNITS[units].value)
        noise = np.random.normal(frames) if type == "gaussian" else np.random.uniform(size=frames, low=-1, high=1)
        w = Wave(rate=rate, sound=noise)
        Wave.display(w, plot=plot, listen=listen)
        return w

    ##### Display functions ######################################################################
    def show(self):
        """
        Display Wave parameters
        """
        return f"""
Wave Object
\tNumber of Samples:\t{self.frames}
\tDuration (seconds):\t{self.duration}
\tSamplingrate (Hertz):\t{self.rate}
\tPCM (integer format):\t{self.pcm}
\tBit (8/16/24/32/64):\t{self.bit}\n
\tsound: {self.sound}\n
"""

    def display(wave, plot=False, listen=False):
        """
        Plot and/or play the wave
        Args:
            wave (Wave): Wave object to deal with
            plot (bool, optional): plot the Wave in a new window. Defaults to False.
            listen (bool, optional): play the Wave throught the PC speakers. Defaults to False.
        """
        if plot:
            wave.oscillo()
        if listen:
            wave.play()

    def plot(self, times=None, title=None, figsize=(10, 10), ylabel=None, xlabel=None, from_=None, to_=None, units="seconds"):
        """
        Create a matplot plot
        Args:
            times (_type_, optional): _description_. Defaults to None.
            title (_type_, optional): _description_. Defaults to None.
            figsize (tuple, optional): _description_. Defaults to (10, 10).
            ylabel (_type_, optional): _description_. Defaults to None.
            xlabel (_type_, optional): _description_. Defaults to None.
        """
        duration, sound = self.limits(from_, to_, units)
        if not times:
            times = np.linspace(0, duration, num=len(sound))
        plt.figure(figsize=figsize)
        if title:
            plt.title(title)
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.xlim(0, duration)
        plt.plot(times, sound)
        plt.show()

    def oscillo(self, times=None, title="Oscillo", figsize=(10, 10), fastdisp=False, from_=None, to_=None, units="seconds"):
        """
        display ocsilogram of the Wave
        Args:
            times (_type_, optional): _description_. Defaults to None.
            title (_type_, optional): _description_. Defaults to None.
            figsize (tuple, optional): _description_. Defaults to (10, 10).
        """
        w = self.cut(from_=from_, to_=to_, units=units) if from_ or to_ else self
        if fastdisp and w.frames > 20000:
            w = w.resample(round(w.frames / (2 ** round(math.log2(round(w.frames / 20000))))))
        w.plot(times=times, title=title, figsize=figsize, ylabel="Amplitude", xlabel="Time (s)")

    def spectro(self, palette="viridis", title="Spectrogram", figsize=(10, 10), cmaplimits=None, from_=None, to_=None, units="seconds"):
        """
        Display a spectrogram of the Wave
        Args:
            palette (_type_, optional): _description_. Defaults to "viridis".
            title (_type_, optional): _description_. Defaults to None.
            figsize (tuple, optional): _description_. Defaults to (10, 10).
            cmaplimits (_type_, optional): _description_. Defaults to None.
        """
        duration, sound = self.limits(from_, to_, units)
        plt.figure(figsize=figsize)
        plt.title(title)
        plt.xlabel("Time [s]")
        plt.ylabel("Frequency (Hz)")
        plt.xlim(0, duration)
        plt.specgram(sound, Fs=self.rate, cmap=palette, vmin=cmaplimits[0] if cmaplimits else None, vmax=cmaplimits[1] if cmaplimits else None)
        plt.colorbar()
        plt.show()

    def play(self, samplerate=None, from_=None, to_=None, units="seconds"):
        """
        Play the sound
        Args:
            samplerate (float, optional): sample rate. Defaults to None.
        """
        _, sound = self.limits(from_, to_, units)
        sd.play(sound, samplerate=samplerate)
        sd.wait()
        sd.stop()

    ##### Wave "arithmetics" ############################################################
    def limits(self, from_=None, to_=None, units="seconds"):
        from_frame, to_frame = self.lm(from_, to_, units, rate=self.rate, frames=self.frames)
        duration = to_ - from_
        sound = self.sound[from_frame:to_frame]
        return duration, sound

    def lm(from_=None, to_=None, units="seconds", rate=None, frames=None):
        u = Wave.UNITS[units].value
        from_frame = round(from_ * u * rate) if from_ else 0
        to_frame = round(to_ * u * rate) if to_ else frames
        if from_frame >= frames:
            from_frame = 0
        if to_frame > frames:
            to_frame = frames
        return from_frame, to_frame

    def concatenate(waves):
        """
        concatenates all waves in list
        Args:
            waves (list(Wave)): list of Waves to concatenate
        Returns:
            Wave
        """
        w = waves[0].newWave()
        for i in range(1, len(waves)):
            if waves[i].rate != w.rate:
                waves[i].resample(w.rate)
        sound = np.hstack([wave.sound for wave in waves])
        w.set(sound=sound)
        return w

    def __add__(self, s):
        w = self
        w += s
        return w

    def __iadd__(self, s):
        if isinstance(s, np.ndarray):
            self.sound += s
        elif isinstance(s, (int, float)):
            self.sound += s
        return self

    def __sub__(self, s):
        w = self
        w.sound -= s
        return w

    def __isub__(self, s):
        self.sound -= s
        return self

    def __mul__(self, s):
        w = self
        w.sound *= s
        return w

    def __imul__(self, s):
        self.sound *= s
        return self

    def __div__(self, s):
        w = self
        w.sound /= s
        return w

    def __idiv__(self, s):
        self.sound /= s
        return self

    def __pow__(self, s):
        w = self
        w.sound**s
        return w

    def __ipow__(self, s):
        self.sound **= s
        return self

    def max(self):
        return np.nanmax(self.sound)

    def mean(self):
        return np.nanmean(self.sound)

    ##### Wave manipulations ############################################################
    def resample(self, newrate, plot=False, listen=False):
        if newrate == self.rate:
            w = self
        else:
            newframes = round(self.duration * newrate)
            old = np.linspace(0, self.frames, self.frames)
            new = np.linspace(0, newframes, newframes)
            w = self.newWave(sound=np.interp(new, old, self.sound))
        Wave.display(w, plot=plot, listen=listen)
        return w

    def change_dB(self, dB, plot=False, listen=False):
        """
        changes the recording's amplitude in dB
        (to remove or add dBs, no need to calculate the equivalent FS value)
        L. Framond, Apr 2021
        Args:
            dB (float): the number of dB to add (positive value) or remove (negative value)
            plot (bool, optional): display resulting Wave. Defaults to False.
            listen (bool, optional): play resulting Wave. Defaults to False.
        Returns:
            Wave
        """
        m = np.nanmax(self.sound)
        curdB = round(20 * math.log10(m), 1)
        newdB = curdB + dB
        newpercent = 10 ** round(newdB / 20)
        w = self.normalize(unit="1", level=newpercent)
        Wave.display(w, plot=plot, listen=listen)
        return w

    def normalize(self, unit="1", center=True, level=1, rescale=True, plot=False, listen=False):
        w = self
        if unit not in ["1", "8", "16", "24", "32", "64", "0"]:
            raise ValueError(
                "'unit' must be either 1 (real valued norm.), 8 (norm. to 8-bit), 16 (norm. to 16-bit), 24 (...), 32 (integer or real valued norm., depends on pcm), or 64 (real valued norm.)"
            )
        if unit == "64" and self.pcm:
            w.pcm = False
        if unit in ["8", "16", "24"] and not self.pcm:
            w.pcm = True
        if center:
            w -= np.mean(w.sound)
        if w.bit == 8 and all(w.sound >= 0):
            w -= 127
        if unit != "0":
            limits = [1, 128, 32768, 8388608, 2147483648]
            m = 1
            if rescale:
                m = np.nanmax(self.sound)
            elif self.pcm:
                m = limits[int(w.bit / 8)]
            if m != 0:
                w *= level / m
            if self.pcm:
                u = int(unit)
                mult = None if u == 1 else (limits[int(u / 8)], 127 if unit == "8" else 0)
                if mult:
                    w.sound = np.round((w.sound * mult[0]) + mult[1])
        w.bit = 32 if unit == "1" else int(unit)

        Wave.display(w, plot=plot, listen=listen)
        return w

    def split(self, at=0, units="seconds", plot=False, listen=False):
        at *= Wave.UNITS[units].value
        if at > self.duration:
            raise ValueError("cannot split outside")
        pos = round(self.rate * at)  # if at != 0 else 1)
        w1 = self.newWave(sound=self.sound[:pos])
        w2 = self.newWave(sound=self.sound[pos:])
        Wave.display(w1, plot=plot, listen=listen)
        Wave.display(w2, plot=plot, listen=listen)
        return w1, w2

    def cut(self, from_=None, to_=None, units="seconds", plot=False, listen=False):
        """
        Extract a subset of a Wave
        Args:
            wave: original wave
            from_: starting time. Defaults to None.
            to_: ending time. Defaults to None.
            units: one of seconds, minutes, hours. Defaults to "seconds".
        Returns:
            Wave dataclass
        """
        if from_ is not None and from_ >= self.duration:
            w = self
        else:
            from_frame, to_frame = self.lm(from_, to_, units)
            w = self.newWave(sound=self.sound[from_frame:to_frame])
        Wave.display(w, plot=plot, listen=listen)
        return w

    def fade(self, din=0, dout=0, plot=False, listen=False):
        if din == 0 and dout == 0:
            w = self
        else:
            ndin = round(din * self.rate)
            ndout = round(dout * self.rate)
            if ndin + ndout > self.frames:
                raise ValueError("The sum of fade in and fade out durations cannot be longer than wave length.")
            if ndin > self.frames:
                raise ValueError("Fade in duration cannot be longer than wave length.")
            if ndout > self.frames:
                raise ValueError("Fade in duration cannot be longer than wave length.")

            sound = self.sound / self.sound.max()
            IN = np.linspace(0, 1, num=ndin)
            OUT = np.linspace(1, 0, num=ndout)
            MID = np.full((len(sound) - (len(IN) + len(OUT)),), 1.0)
            FADE = np.concatenate([IN, MID, OUT])
            sound = np.array([wa * fa for wa, fa in zip(sound, FADE)])
            sound = sound / max(abs(sound))

            w = self.newWave(sound=sound)
        Wave.display(w, plot=plot, listen=listen)
        return w

    def addsil(self, at="end", d=None, plot=False, listen=False):
        if at not in ["start", "middle", "end", "choose"]:
            raise ValueError("at must be one of start, middle, end or choose")
        if not d:
            raise ValueError("silence duration has to be set with the argument 'd'")
        sil = self.newWave(sound=np.zeros((round(d * self.rate),)))
        match at:
            case "start":
                w = Wave.concatenate([sil, self])
            case "end":
                w = Wave.concatenate([self, sil])
            case "middle":
                w1, w2 = self.split(at=self.frames / (2 * self.rate))
                w = Wave.concatenate([w1, sil, w2])
            case "choose":
                print("choose position on the wave\n")
                self.oscillo()
                # coord=locator(n=1)
                # at=coord[x[1]]
                # abline(v=at,col=2,lty=2)
                w = self

        Wave.display(w, plot=plot, listen=listen)
        return w
