#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Common procedures to treat CITs"
__contact__ = "framondj@gmail.com"


import math

# library(dplyr)
# library(reshape2)
# pip install pandas
import pandas as pd

import call_analysis

# library(ggplot2)
# pip install ggplot
import ggplot
import matplotlib as plt
import math

# library(seewave)
# library(tuneR)
import wave
import r_func
### SETTINGS  ###########################################################################
# sample rate
f=44100

# buffer time before and after a call was detected (in SAMPLES)
buf = 2250

# spectrogram window size (SAMPLES)
ws = 256

# smoothing of envelope (in SECONDS)
dur_smooth = 0.005
sm = f*dur_smooth

# dB threshold to define call duration and frequency max and min
ana_thr = [-12,-6]

# list of logs
logcoldBs = [math.log(x)*15-math.log(20)*15 for x in range(1,20)]


##################################################
def all_call_pars(session, meta):
    filename = f"../call_analysis/to_check_{session}.csv"
    all_call_pars = pd.read_csv(filename)

    supfrq = max(all_call_pars['hiF_kHz_12dB'])+ 1.5
    
    for m in meta:
        if m['Session'] != session:
            continue
        # read the recording + print spectrogram  
        rec_name = f"./WAV/{m['Session']}/{m['Recording_nb']}.WAV"
        wav = wave.open(rec_name, 'r')
        dur, rec = call_analysis.readWave(rec_name, from_=m['sec_rec_start'], to_=m['sec_rec_end'], units="seconds")
        
        call_params = all_call_pars[all_call_pars['recnb'] == m['Recording_nb']]

        ncols = len(call_params[1,])
        call_timestamps = all_call_pars[,(ncols-1):ncols]
    
    
    # call check: 
    # for each detected call... 
    call_params["exclude"] = 0
    call_params["add_filt"] = 0
    # X11()
    j=2
    print("for each call, type `e` to exclude the call from the analysis, a number to perform additional filtering, or `enter` if the call is fine")
    for(j in 1:length(call_timestamps[,1])){
        
        # snip it out
        call = rec@left[call_timestamps[j,1]:call_timestamps[j,2]]
        call_filt = firLena(call, f=f, from = call_params$filt[j])
        call_env = env(call, plot = F, msmooth = c(sm,50), norm = T, f=f)
        env_plot = data.frame(x = seq(0, duration(call, f=f), length.out = length(call_env)),
                            y = call_env)
        
        # plot
        tit = paste0("  Call n°", j , "/", length(call_timestamps[,1]))
        print(plot_call_analysis(call_filt, call_params, call_id = j, ymax=supfrq)+
                ggtitle(tit)+
                geom_line(data = env_plot, aes(x = x, y=y), inherit.aes = F , color = "white"))
        
        
        # choose if the call is OK or not
        var = readline(prompt = "is the call ok? ")
        
        # exclude non calls
        if(length(var) > 0){
        if(var == "e"){
            call_params$exclude[j] = 1
        }
        var = as.numeric(var)
        ok = is.na(var)
        
        # additional filtering + reanalysis
        if(!ok){
            var = as.numeric(var)
            if(var<call_params$filt[j]|var>10000):
                cor = readline(prompt = paste0("WARNING!! are you sure tthat you want to use a filter of ", var, " Hz? "))
                cor = as.numeric(cor)
                ok = is.na(cor)
                if(!ok){
                var = cor
                }
            
        
            # filter
            call2 = firLena(call, f=f, from = var, output = "Wave")
            
            # reanalyse
            tmsp = data.frame(starts = 1,ends = length(call2))
            newpars = analyse_calls(call2, timestamps = tmsp, dB_thr = ana_thr)
            call_params[j,1:length(newpars)] = newpars
            
            # replot
            call_env = env(call2, plot = F, msmooth = c(sm,50), norm = T, f=f)
            env_plot = data.frame(x = seq(0, duration(call, f=f), length.out = length(call_env)),
                                y = call_env)
            
            print(plot_call_analysis(call_filt, call_params, call_id = j, ymax = supfrq)+
                    ggtitle(tit)+
                    geom_line(data = env_plot, aes(x = x, y=y), inherit.aes = F , color = "white"))
            call_params$add_filt[j] = var
            
            # if now good, keep, otherwise exclude
            var = readline(prompt = "and now ? ")
            if(var == "e"){
            call_params$exclude[j] = 1
            }else{
            var = as.numeric(var)
            ok = is.na(var)
            if(!ok){
                call_params$exclude[j] = 1
            }
            }
            
        }
        }
    }
    # call_params = call_params[call_params$exclude==0,]
    
    return(call_params)
    
    })

    call_check = do.call(rbind, call_check)

    # save results
    filename = f"../call_analysis/checked_{session}.csv"
    if call_check.to_csv(filename, encoding='utf-8', index=False, header=False):
        print("file saved!")