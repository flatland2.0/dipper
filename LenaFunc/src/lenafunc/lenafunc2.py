## Useful functions

# This is an aggreagation of different functions written over the years, 
# instead of having them all at the beginning of each code, which is messy and difficult to find back.

# Seewiesen  July, 9th 2021
import math
import regex
import datetime
import wawe

import pandas as pd
import matplotlib as plt
import numpy as np
import math

##################################################
def select(options, question):
    df = isinstance(options, pd.DataFrame)
    opts = options[0].values if df else options.values
    txt = options[1].values if df else None
    done = False
    while not done:
        print("\n".join([f"{o}: {txt[i] if df else ''}" for i, o in enumerate(opts)]))
        if (response := input(f"{question} (Q to quit): ")).upper()[0] == "Q":
            exit(0)
        if response not in opts:
            print("\n\nPlease select from the list of choices")
        else:
            done = True
    return response

def spectro(data, palette=None, collevels=None, flim=None):
    if not palette:
        palette = "gray"
    # Matplotlib.pyplot.specgram() function to generate spectrogram
    # spectro(rec_name, fastdisp=True, palette=reverse.gray.colors.1,  collevels=cl, flim=c(0, 10))
    plt.specgram(data, Fs=f, cmap=palette)
    plt.title("Spectrogram Using matplotlib.pyplot.specgram() Method")
    plt.xlabel("Time [s]")
    plt.ylabel("Frequency")
    plt.show()
    
def readWave(rec_name, from_=None, to_=None, units="seconds"):
    """
    simulate the behavior of the tuneR readWave function
    Args:
        rec_name: wave file name
        from_: starting time. Defaults to None.
        to_: ending time. Defaults to None.
        units: one of seconds, minutes,hours. Defaults to "seconds".

    Returns:
        tuple with the duration of the wav and the data
    """
    wav = wave.open(rec_name, "r")
    if units == "minutes":
        u = 60
    elif units == "hours":
        u = 3600
    else:
        u = 1
    nframes = wav.getnframes()
    nchan = wav.getnchannels()
    sw = wav.getsampwidth()
    rat = wav.getframerate()
    tdur = nframes / rat
    from_frame = int(from_ * u * rat) if from_ else 0
    to_frame = int(to_ * u * rat) if to_ else nframes
    n = to_frame - from_frame
    dur = n / rat
    wav.setpos(from_frame)
    wavFrames = wav.readframes(n)
    ys = np.frombuffer(wavFrames, dtype=np.int16)

    # print(nframes, nchan, sw, rat, tdur, dur)

    return dur, ys

def beep(f=1000, d=1):    
    # generates a beep with a 10% fade in and 10% fade out
    # f = frequency of the beep in Hz
    # d = duration of the beep in seconds
    # based on the package seewave. L. Framond, July 2021.
    audio = []
    sample_rate= 44100
    num_samples = int(d * (sample_rate / 1000.0))
    fadedur = .1 * num_samples
    for x in range(int(num_samples)):
        if x < fadedur:
            vol = x / fadedur
        elif x > num_samples - fadedur:
            vol = (1 - x) / fadedur
        else:
            vol = 1.0
        audio.append(vol * math.sin(2 * math.pi * f * (x / sample_rate)))
    return audio

# def beep(f=1000, d=1):
#     # generates a beep with a 10% fade in and 10% fade out
#     # f = frequency of the beep in Hz
#     # d = duration of the beep in seconds
#     # based on the package seewave. L. Framond, July 2021.
#     fadedur = .1 * d
#     bip = synth(f = 44100, d = d, cf = f, output = "Wave")
#     bip = fadew(bip, din = fadedur, output = "Wave")
#     bip = fadew(bip, dout = fadedur, output = "Wave")
#     return bip


def change_dB(sound, dB):
    # changes the recording's amplitude in dB 
    # (to remove or add dBs, no need to calculate the equivalent FS value)
    # sounds =  a "wave" File
    # dB = the number of dB to add (positive value) or remove (negative value)
    # L. Framond, Apr 2021
    current_dB = round(20*math.log10(max(sound@left)), 1)
    newdB = current_dB + dB
    newpercent = 10^(newdB/20)
    rescaled = normalize(sound, unit = "1", level = newpercent)
    return(rescaled)



def make_calib_pb(freqs, d, SNR = 0):
  # generates a playbacks of beeps for calibration
  # freqs = a vector of frequencies to use
  # d = duration of the beeps
  # SNR = if the playback should have some noise in the background, choose the SNR here. default = 0
  # based on package seewave. L. Framond, June 2021.
    nbeeps = len(freqs)
    
    pb = beep(f=freqs[1], d = d)
    
    for i in range(2, nbeeps):
        newbeep = beep(f = freqs[i], d = d)
        newbeep = addsilw(newbeep, at = "start", d = d*2, output = "Wave")
        pb = pastew(newbeep, pb, at = "end", output = "Wave")
  
    if SNR != 0:
        durpb = duration(pb)
        noise = noisew(f = 44100, d = durpb, output = "Wave")
        noise = fadew(noise, din = .1, output = "Wave")
        noise = fadew(noise, dout = .1, output = "Wave")
        noise = change_dB(noise, dB=(-1*SNR))
        pb@left = pb@left+noise@left
  
    pb = addsilw(pb, at = "start", d = d, output = "Wave")
    pb = addsilw(pb, at = "end", d = d, output = "Wave")
    
    return pb



def RMS_dB(sound):
    # Calculate the RMS level of a sound /!\  do not provide the "Wave", only the values!
    # L. Framond, June 2021.
    max_ = max(sound)
    if max_>1:
        ref = (2^15) - 2
    else:
        ref = 1
    
    rms = math.sqrt(mean(sound^2))
    rms_dB = 20*math.log10(rms/ref)
    return rms_dB

def f_aa( freq, T, RH, p=101325):
    # Holger's fuction to calculate atmospheric attenuation
    # 1) freq: frequency (Hz)
    # 2) T: temperature (degree C)
    # 3) RH: relative humidity (%)
    # 4) p (optional): pressure (Pa) (standard value: 101325)
    # H.Goerlitz, 2019.
    Ta = T+273.15       # convert to Kelvin
    Tr = Ta/293.15      # convert to relative air temperature (re 20 deg C)
    pr = p/101325       # convert to relative pressure
    C = 4.6151 - 6.8346 * ((273.16/Ta)^1.261) 
    h = RH * 10^C / pr
    frO = pr * ( 24+4.04e4 * h * (0.02+h) / (0.391+h) )
    frN = pr * (Tr^(-0.5)) * (9+280*h*math.exp(-4.17*((Tr^(-1/3))-1)))
    alpha = freq * freq *
        ( 1.84e-11 * (1/pr) * math.sqrt(Tr) +
            (Tr^(-2.5)) * ( 0.01275 * (math.exp(-2239.1/Ta)*1/(frO+freq*freq/frO)) +
                            0.1068*(math.exp(-3352/Ta)*1/(frN+freq*freq/frN)) ) )
    AA = 8.686 * alpha         # convert to dB (lg(x/x0)) from Neper (ln(x/x0)).
    return AA

def sub_D_range(x):
    Fp_index = which.min(abs(ambient_spectrum[,1] -(1000*bird_Fp[x])))
    ambient_dB_peak = ambient_spectrum[Fp_index,2]
    
    test_dist = seq(1,500, .25)
    alpha = f_aa(bird_Fp[x]*1000, 10,50)
    AA = alpha*test_dist
    GA = 20*math.log10(test_dist)
    RL = bird_SL[x]-AA-GA
    index = which.min(abs(RL-(ambient_dB_peak+4)))
    dist=test_dist[index]
    
    return [ambient_dB_peak, dist, GA[index], alpha, RL[index]]
    
def D_range(bird_SL, bird_Fp, ambient_spectrum, thr = 4):
    # Caluclate the detection range of a bird based on its source level, peak frequency and the ambient noise.
    # Assumption: we need to be 4 dB above the ambient noise to detect the call.
    # Bird_SL = Source level of the bird (dB SPL re 20 microPa @ 1 m)
    # Bird_Fp = peak frequency of song (kHz)
    # ambient spectrum = data frame with two columns: col 1 = frequencies, 
    # col 2 = corresponding amplitude of the ambient noise at this frequency
    # Thr = threshold for detection
    # L.Framond, Apr 2021.
    
    # bird_SL=100 
    # bird_Fp=5
    # ambient_spectrum = data.frame(spectrum_ambient_norm$Freq,spectrum_ambient)
    # 
    n = len(bird_SL)
    x = 1
    sp_mask = map([sub_D_range(x) for x in range(1,n)])
    sp_mask = as.data.frame(do.call(rbind, sp_mask))
    colnames(sp_mask) = c("ambient_dB_peak", "dist", "GA", "alpha", "bird_RL")
    dB_sp = data.frame(bird_Fp, bird_SL, sp_mask)
    
    return dB_sp


def undB(val, ref=(20*10^(-6))):
    # takes a value in decibel and transforms it into original scale. 
    # ref = reference of the decibel. defauls = 20 micropa
    # L. Framond, June 2021. 
    undB = ref*10^(val/20)
    return undB


def dB(val, ref=(20*10^(-6))):
    # transforms in dB. ref is 20 micropa
    # L. Framond, July 2021.
    dB = 20*math.log10(val/ref)
    return dB

def get_character_n(x, n, nchar):
    # extracts a certain character or character string from a long string
    # x = original string
    # n = place of the first character to extract
    # length of the character string to extract
    # L. Framond, based on some code from stack exchange, 2021
    return x[n:n+nchar]


def read_AM(names, date=True, time=True):
    # extract date and time from the audiomoth file names
    # names = vector with all the names. 
    # date and time, if we only want the date or only the time
    # L. Framond, July 2021.
    # name format YYYMMDD-hhmmssxxxxxxx
    rex = r"(\d]{4})(\d{2})(\d{2})-(\d{2})(\d{2})(\d{2})"
    out = []
    for n in names:
        if m := regex.search(rex, n):
            df = {}
            if date:
                df['year'] = m.groups(1)
                df['month'] = m.goups(2)
                df['day'] = m.groups(3)
            if time:
                df['hour'] = m.groups(4)
                df['min'] = m.groups(5)
                df['sec'] = m.groups(6)
            out.append(df) 
    return out


def calc_egg_mass(length, breadth):
    # based on Hoyt, 1979
    # egg length and largest diameter in mm, returns mass in g
    # L. Framond, 03.2021
    return 0.548*(length/10)*((breadth/10)^2)


def complete_data(avrg, min, max):
    # function that completes the column with average data with the mean of the range data when available
    # avrg = vector of average values
    # min, max = vectors with the range
    # L. Framond, 03.2021
    
    eggs = data.frame(avrg = is.na(avrg), max = is.na(max))
    dat = []
    for e in eggs:
        if e['avrg']==True:
            if e['max']==True:
                dat = None
            else:
                dat = (max[x]+ min[x])/2
        
        else:
            dat = avrg[x]
        
        
    return dat
        
    # sapply(1:length(avrg), function(x){
    #     if(eggs$avrg[x]==True){
    #         if(eggs$max[x]==True){
    #             dat = NA
    #         }else{
    #             dat = (max[x]+ min[x])/2
    #         }
    #     }else{
    #         dat = avrg[x]
    #     }
    #     return dat
    # })



def ribon2(mod, dat, predic):
    ## function that calculates and gives values for plotting the fitted line and 95% interval of the 
    ## phyloglm model, using the result of boostraping
    # works with phyloglm results
    # mod = results of the model
    # dat = dataset
    # predic = character string identifying the predictor variable
    # returns a big dataframe that can be plotted in ggplot2
    # L. Framond, 03.2021
    
    # mod = mass_phy
    # dat = mass_dat
    # predic = "hab_scores"
    
    CI95 = confint(mod)
    CI95 = cbind(mod$bootconfint95[1,], mod$bootconfint95[2,])
    colnames(CI95) = c("2.5 %", "97.5 %")
    beta = mod$coefficients
    
    predic_dat = predic
    if(predic == "log(g_mean_dunning)"){
        predic_dat = "g_mean_dunning"
        dat[, predic_dat] = log(dat[, predic_dat])
    }
    
    xstart = min(dat[, predic_dat])
    xend = max(dat[, predic_dat])
    
    Xs = seq(xstart, xend, length.out = 50)
    avrg = plogis(beta["(Intercept)"] + beta[predic] * Xs)
    
    low = numeric()
    hig = numeric()
    
    j=1
    for (j in 1:50){
        low[j] = min(plogis(beta["(Intercept)"] + beta[predic] * Xs[j]), 
                    plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]), 
                    plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]), 
                    plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]))
        
        
        hig[j] = max(plogis(beta["(Intercept)"] + beta[predic] * Xs[j]), 
                    plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]), 
                    plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]), 
                    plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                    plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]))
    }
    
    return(data.frame(X = Xs, avrg, low, hig))


def lat_deg2m(lats):
    # convert latitude into meters
    out = []
    for lat in lats:
        lat_rad = lat*math.pi/180
        onedeg = 111132.92 - 559.82*math.cos(2*lat_rad) + 1.175*math.cos(4*lat_rad) - 0.0023*math.cos(6*lat_rad)
        out.append(onedeg * lat)
    return out


def long_deg2m(longs):
    # convert longitude into meters
    out = []
    for lg in longs:
        lg_rad = lg*math.pi/180
        onedeg  = 111412.84*math.cos(lat_rad) + 93.5*math.cos(3*lat_rad) - 0.118*math.cos(5*lat_rad)
        out.append(onedeg*lg)
    return out

def sun(lat_, ju_):
    # https://gml.noaa.gov/grad/solcalc/calcdetails.html
    g = 2*math.pi/365*(ju_-1)
    eqtime = 229.18*(0.000075+0.001868*math.cos(g)-0.032077*math.sin(g)-0.014615*math.cos(2*g)-0.040849*math.sin(2*g))
    eqtime2 = 180*eqtime/math.pi
    decl = 0.006918-0.399912*math.cos(g)+0.070257*math.sin(g)-0.006758*math.cos(2*g)+0.000907*math.sin(2*g)-0.002697*math.cos(3*g)+0.00148*math.sin(3*g)
    decl2 = 180*decl/math.pi
    ha = math.acos((math.cos(90.833*math.pi/180)/(math.cos(lat_*math.pi/180)*math.cos(decl)))-(math.tan(lat_*math.pi/180)*math.tan(decl)))
    ha2 = ha*180/math.pi
    return eqtime, ha2

def sunrise(lats, longs, jus):
    S = []
    for lat_, long_, ju_ in zip(lats,longs,jus):
        eqtime, ha2 = sun(lat_, ju_)
        stime = (720-4*(long_+ha2)-eqtime)/60
        S.append(stime)
    return S


def sunset(lats, longs, jus):
    S = []      
    for lat_, long_, ju_ in zip(lats,longs,jus):
        eqtime, ha2 = sun(lat_, ju_)
        stime = (720-4*(long_-ha2)-eqtime)/60
        S.append(stime)
    return S


def Julian(year, month, day):
    J = sapply(1:length(day), function(x){
        ndays = data.frame(month = 0:12,
                        ndays = c(0,30,28,31,30,31,30,31,31,30,31,30,31))
        Jul = sum(ndays$ndays[1:(month[x])])+day[x]
        if (year[x]%%4==0&month[x]>2){
        Jul = Jul+1
        }
        return(Jul)
    })
    return(J)


# This is based on the the Navy's Astronomical Equation found here and verified with their own calculator: http://aa.usno.navy.mil/faq/docs/JD_Formula.php
def get_julian_datetime(date):
    """
    Convert a datetime object into julian float.
    Args:
        date: datetime-object of date in question

    Returns: float - Julian calculated datetime.
    Raises: 
        TypeError : Incorrect parameter type
        ValueError: Date out of range of equation
    """

    # Ensure correct format
    if not isinstance(date, datetime.datetime):
        raise TypeError('Invalid type for parameter "date" - expecting datetime')
    elif date.year < 1801 or date.year > 2099:
        raise ValueError('Datetime must be between year 1801 and 2099')

    # Perform the calculation
    julian_datetime = 367 * date.year 
        - int((7 * (date.year + int((date.month + 9) / 12.0))) / 4.0) 
        + int((275 * date.month) / 9.0) 
        + date.day + 1721013.5 
        + (date.hour 
            + date.minute / 60.0 
            + date.second / math.pow(60,2)) / 24.0 - 0.5 * math.copysign(1, 100 * date.year + date.month - 190002.5
            ) 
        + 0.5
    return julian_datetime

def firLena(wave, f, channel=1, from_=None, to=None, bandpass=True, custom=None, wl=512, wn="hanning", rescale=False,
            listen=False, output="matrix"):

    # wave = noise
    # custom = unfilter
    # wl=512
    # wn="hanning"
    
    input <- inputw(wave = wave, f = f, channel = channel)
    wave <- input['w']
    f <- input['f']
    bit <- input['bit']
    rm(input)
    freq <- seq(0, f/2 - f/wl, length.out = wl/2)
    if not from_:
        from_ = 1
    else:
        from_ <- which.min(abs(freq - from_))
    
    if not to:
        to = wl/2
    
    else:
        to <- which.min(abs(freq - to))
    
    n <- nrow(wave)
    if custom:
        if is.matrix(custom): 
            custom <- custom[, 2]
        if len(custom != wl/2):
            print("custom filter length has to be equal to 'wl'/2")
            exit(1)
        if bandpass:
            filtspec =[custom, rev(custom)]
        
        else:
            filtspec <- 1 - c(custom, rev(custom))
        
        # filtspec <- filtspec/max(filtspec)
    
    else:
        filtspec <- rep(1, wl/2)
        if bandpass:
            filtspec[-(from:to)] <- 0
        else:
            filtspec[from:to] <- 0
        
        filtspec <- c(filtspec, rev(filtspec))
    
    pulse <- Re(fft(filtspec, inverse = True)/length(filtspec))
    # plot(pulse)
    # pulse <- pulse/(2*max(abs(pulse))-sum(abs(pulse)))
    # plot(pulse)
    pulse <- c(pulse[((wl/2) + 1):wl], pulse[-((wl/2 + 1):wl)])
    fl <- n + length(pulse) - 1
    log.fl <- math.log2(fl)
    if log.fl != math.floor(log.fl):
        p <- math.ceil(log.fl)
        nzp <- 2^p - fl
        wave <- as.matrix(c(rep(0, math.floor(nzp/2)), wave[, 1], rep(0, math.ceil(nzp/2))))
    
    W <- ftwindow(wl = wl, wn = wn)
    wave2 <- convolve(wave[, 1], pulse * W, type = "filter")
    n2 <- len(wave2)
    diffn <- n2 - n
    if diffn > 0:
        wave2 <- wave2[math.floor(diffn/2):(n + math.floor(diffn/2) - 1)]
    else:
        wave2 <- c(rep(0, math.floor(abs(diffn/2)) + 1), wave2, rep(0, math.floor(abs(diffn/2))))
    
    wave2 <- wave2 - mean(wave2, na.rm=True)
    if rescale:
        wave2 <- rescale(wave2, lower = min(wave), upper = max(wave))
    
    wave2 <- outputw(wave = wave2, f = f, format = output)
    if listen:
        listen(wave2, f = f)
    
    return(wave2)

def readWave(rec_name, from_=None, to_=None, units="seconds"):
    """
    simulate the behavior of the tuneR readWave function
    Args:
        rec_name: wave file name
        from_: starting time. Defaults to None.
        to_: ending time. Defaults to None.
        units: one of seconds, minutes,hours. Defaults to "seconds".

    Returns:
        tuple with the duration of the wav and the data
    """
    wav = wave.open(rec_name, "r")
    if units == "minutes":
        u = 60
    elif units == "hours":
        u = 3600
    else:
        u = 1
    nframes = wav.getnframes()
    nchan = wav.getnchannels()
    sw = wav.getsampwidth()
    rat = wav.getframerate()
    tdur = nframes / rat
    from_frame = int(from_ * u * rat) if from_ else 0
    to_frame = int(to_ * u * rat) if to_ else nframes
    n = to_frame - from_frame
    dur = n / rat
    wav.setpos(from_frame)
    wavFrames = wav.readframes(n)
    ys = np.frombuffer(wavFrames, dtype=np.int16)

    # print(nframes, nchan, sw, rat, tdur, dur)

    return dur, ys


def spectro(data, palette=None):
    if not palette:
        palette = "gray"
    # Matplotlib.pyplot.specgram() function to generate spectrogram
    # spectro(rec_name, fastdisp=True, palette=reverse.gray.colors.1,  collevels=cl, flim=c(0, 10))
    plt.specgram(data, Fs=f, cmap=palette)
    plt.title("Spectrogram Using matplotlib.pyplot.specgram() Method")
    plt.xlabel("Time [s]")
    plt.ylabel("Frequency")
    plt.show()