#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Common procedures to treat CITs"
__contact__ = "framondj@gmail.com"

# import datetime
# import io
import logging

# import os
# import sys
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import jflogging
from scipy.stats import binomtest
from scipy.stats import binomtest
from scipy.fft import fft, ifft
from scipy import signal
from lenawave import *

# import openpyxl as xl
# import regex
# import scipy.interpolate as interp
# from scipy.io import wavfile
# from dataclasses import dataclass
# from os.path import dirname
# from os.path import join as pjoin
# import scipy.io

lgr = jflogging.Logger(name="R")
lgr.set_debug(True)
lg = logging.getLogger()

# R ncol(df) :Python len(df.columns)
# R nrow(df) :Python len(df)
# R dim(df) :Python df.shape
# R x <- data.frame(X = c(1, 2, 3, 4, 5)) :Python x = pd.DataFrame({X: [1, 2, 3, 4, 5]})
# R x <- seq(start, stop, by) : Python x = np.arange(1, 500, 0.25)


#### Functions ###############################################################################
def seq(start, stop, step):
    return np.arange(start, stop + step, step)


def beep(f=1000.0, d=1.0, rate=44100, units="seconds", plot=False, listen=False):
    """
    generates a beep with a 10% fade in and 10% fade out
    based on the package seewave. L. Framond, July 2021.
    Args:
        f (float, optional): frequency of the beep in Hz. Defaults to 1000.0.
        d (float, optional): duration of the beep in seconds. Defaults to 1.0.
        rate (int, optional): sample rate. Defaults to 44100.
        units (str, optional): units for duration. one of hours, minutes or seconds. Defaults to seconds
        plot (bool, optional): plot the beep. Defaults to False.
        listen (bool, optional): play th beep. Defaults to False.
    Returns:
        Wave object
    """
    w = Wave.synth(f, d, rate, units=units)
    fadedur = 0.1 * d * Wave.UNITS[units].value
    w = w.fade(din=fadedur, dout=fadedur)
    Wave.display(w, plot=plot, listen=listen)
    return w


def make_calib_pb(freqs, d, rate=44100, SNR=0, units="seconds", plot=False, listen=False):
    """
    generates a playbacks of beeps for calibration
    based on package seewave. L. Framond, June 2021.
    Args:
        freqs (list): a vector of frequencies to use
        d (int): duration of the beeps
        rate (int, optional): sample rate. Defaults to 44100.
        SNR (int, optional): if the playback should have some noise in the background, choose the SNR here. Defaults to 0.
        units (str, optional): units for duration. one of hours, minutes or seconds. Defaults to seconds
    Returns:
        Wave object
    """
    sil = Wave(rate=rate, sound=np.zeros((round(d * rate),)))
    if SNR != 0:
        noise = Wave.noise(rate=44100, d=d, units=units)
        noise.oscillo(title="noise1")
        noise = noise.fade(din=0.1, dout=0.1).change_dB(dB=(-1 * SNR))
        noise.oscillo(title="noise2")
    beeps = []  # [beep(f=freq, d=d) for freq in freqs]
    for f in freqs:
        b = beep(f=f, d=d)
        if SNR != 0:
            b += [noise]
        beeps.append(Wave.concatenate([sil, b, sil]))
    w = Wave.concatenate(beeps)
    Wave.display(w, plot=plot, listen=listen)
    return w


def undB(val, ref=20 * 10 ^ (-6)):
    """
    takes a value in decibel and transforms it into original scale.
    L. Framond, June 2021.
    Args:
        val (float): dB value
        ref (float, optional): reference of the decibel. Defaults to 20*10^(-6).micropa
    """
    return ref * 10 ^ (val / 20)


def dB(val, ref=20 * 10 ^ (-6)):
    """
    # transforms in dB. ref is 20 micropa
    # L. Framond, July 2021.
    Args:
        val (float): dB value
        ref (float, optional): reference of the decibel. Defaults to 20*10^(-6) micropa
    """
    return 20 * math.log10(val / ref)


def RMS_dB(sound):
    """
    Calculate the RMS level of a sound
    L. Framond, June 2021.
    Args:
        sound (Wave): Wave to treat
    Returns:
        float: dB value
    """
    ref = (2**15) - 2 if sound.max() > 1 else 1
    rms = math.sqrt((sound**2).mean())
    return 20 * math.log10(rms / ref)


def f_aa(freq, T, RH, p=101325):
    """
    Holger's function to calculate atmospheric attenuation
    # H.Goerlitz, 2019.
    Args:
        freq (float): frequency (Hz)
        T (float): temperature (degree C)
        RH (float): relative humidity (%)
        p (int, optional): pressure (Pa) (standard value: 101325). Defaults to 101325.

    Returns:
        float: atmospheric attenuation
    """
    Ta = T + 273.15  # convert to Kelvin
    Tr = Ta / 293.15  # convert to relative air temperature (re 20 deg C)
    pr = p / 101325  # convert to relative pressure
    C = 4.6151 - 6.8346 * ((273.16 / Ta) ** 1.261)
    h = RH * 10**C / pr
    frO = pr * (24 + 4.04e4 * h * (0.02 + h) / (0.391 + h))
    frN = pr * (Tr ** (-0.5)) * (9 + 280 * h * math.exp(-4.17 * ((Tr ** (-1 / 3)) - 1)))
    alpha = (
        freq
        * freq
        * (
            1.84e-11 * (1 / pr) * math.sqrt(Tr)
            + (Tr ** (-2.5))
            * (0.01275 * (math.exp(-2239.1 / Ta) * 1 / (frO + freq * freq / frO)) + 0.1068 * (math.exp(-3352 / Ta) * 1 / (frN + freq * freq / frN)))
        )
    )
    AA = 8.686 * alpha  # convert to dB (lg(x/x0)) from Neper (ln(x/x0)).
    return AA


def D_range_one(bird_Fp, bird_SL, ambient_spectrum, thr):
    # check all ambient frequencies against bird peak frequency * 1000 and get minimum
    bFp = bird_Fp * 1000
    idx = np.argmin(abs(ambient_spectrum[0] - bFp))
    ambient_dB_peak = ambient_spectrum[idx][1]
    test_dist = seq(1, 500, 0.25)
    alpha = f_aa(bFp, 10, 50)
    AA = alpha * test_dist
    GA = 20 * np.log10(test_dist)
    RL = bird_SL - AA - GA
    idx = np.argmin(abs(RL - (ambient_dB_peak + thr)))
    dist = test_dist[idx]
    return [bird_Fp, bird_SL, ambient_dB_peak, dist, GA[idx], alpha, RL[idx]]


def D_range(bird_Fp, bird_SL, ambient_spectrum, thr=4):
    """
    Calculate the detection range of a bird based on its source level, peak frequency and the ambient noise.
    Assumption: we need to be 4 dB above the ambient noise to detect the call.
    L.Framond, Apr 2021.
    Args:
        bird_Fp (np.array): peak frequency of song (kHz).
        bird_SL (np.array): Source level of the bird (dB SPL re 20 microPa @ 1 m).
        ambient_spectrum (pd.DataFrame): Pandas DataFrame with two columns:
            col 1 = frequencies,
            col 2 = corresponding amplitude of the ambient noise at this frequency.
        thr (int, optional): threshold for detection. Defaults to 4.
    Returns:
        pandas DataFrame
    """
    # bird_SL  [100, 110, 120],
    # bird_fP [5.0, 5.2, 5.9],
    # amb [[5000, 80], [5100, 70], [5200, 75], [5300, 105], [5400, 85], [5500, 65], [5600, 60], [5700, 105], [5800, 85], [5900, 95]],
    amb = np.array(ambient_spectrum)
    sp_mask = np.array([D_range_one(bird_Fp[x], bird_SL[x], amb, thr) for x in range(len(bird_SL))]).T
    lgr.debug(sp_mask)
    colnames = ["bird_Fp", "bird_SL", "ambient_dB_peak", "dist", "GA", "alpha", "bird_RL"]
    dB_sp = pd.DataFrame.from_dict(dict(zip(colnames, sp_mask)))
    return dB_sp


def plogis(x):
    """
    Cumulative Logistic Density
    Args:
        x (np.array): vector to work on
    Returns:
        np.array
    """
    if not isinstance(x, np.ndarray):
        raise ValueError("parameter must be a vector")
    return np.divide(1, 1 + np.exp(-x))


def confint(mod):
    binomtest(13, 100).proportion_ci()
    return np.array()


def ribon2(mod, dat, predic):
    """
    function that calculates and gives values for plotting the fitted line and 95% interval of the
    phyloglm model, using the result of boostraping
    works with phyloglm results
    # L. Framond, 03.2021
    Args:
        mod (_type_): results of the model
        dat (pd.DataFrame): dataset
        predic (string): character string identifying the predictor variable
    Returns:
        pandas DataFrame: a big dataframe that can be plotted in ggplot2
    """
    # mod = mass_phy
    # dat = mass_dat
    # predic = "hab_scores"

    CI95 = confint(mod)
    # CI95 = cbind(mod$bootconfint95[1,], mod$bootconfint95[2,])

    colnames_CI95 = ["2.5 %", "97.5 %"]
    beta = mod.coefficients

    predic_dat = predic
    if predic == "log(g_mean_dunning)":
        predic_dat = "g_mean_dunning"
    dat[predic_dat] = np.log(dat[predic_dat])

    xstart = min(dat[predic_dat])
    xend = max(dat[predic_dat])

    Xs = np.linspace(start=xstart, stop=xend, num=50)
    avrg = plogis(beta["(Intercept)"] + beta[predic] * Xs)

    low = []
    hig = []

    for j in range(1, 50):
        low[j] = np.min(
            [
                plogis(beta["(Intercept)"] + beta[predic] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
            ]
        )

        hig[j] = np.max(
            [
                plogis(beta["(Intercept)"] + beta[predic] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
            ]
        )

    colnames = ["X", "avrg", "low", "high"]
    ribon = pd.DataFrame.from_dict(dict(zip(colnames, avrg, low, hig)))
    return ribon


def firLena(wave, f, channel=1, from_=None, to_=None, bandpass=True, custom=None, wl=512, wn="hanning", rescale=False, listen=False, output="matrix"):

    # wave = rec
    # # custom = unfilter
    # wl=512
    # wn="hanning"
    # from = 1800
    #
    try:
        # wave = input["w"]
        # bit = wave["bit"]

        freq = np.linspace(start=0, stop=(f / 2) - (f / wl), num=round(wl / 2))
        lgr.debug(round(wl / 2), (f / 2) - (f / wl), freq)
        from_ = round(np.min(abs(freq - from_))) if from_ else 1
        to_ = round(np.min(abs(freq - to_))) if to_ else round(wl / 2)
        lgr.debug(from_, round(np.min(abs(freq - from_))), to_, round(np.min(abs(freq - to_))))
        n = wave.frames
        if custom:
            if custom.shape[1] == 2:
                custom = custom[1]
            if len(custom) != wl / 2:
                raise ValueError("custom filter length has to be equal to 'wl'/2")
            if bandpass:
                filtspec = [custom, custom[::-1]]
            else:
                filtspec = 1 - [custom, custom[::-1]]

        else:
            filtspec = np.repeat(1, wl / 2)
            if bandpass:
                fl = filtspec
                # IN = [i for i in range(len(filtspec)) if i < from_ or i > to_]
                # fl[:, IN] = 0

                filtspec[:from_] = 0
                filtspec[to_:] = 0
                lgr.debug(fl, filtspec)
            else:
                filtspec[from_:to_] = 0

            filtspec = [filtspec, filtspec[::-1]]
        fft = ifft(filtspec) / len(filtspec)
        lgr.debug(fft)
        pulse = fft.real
        lgr.debug(pulse)

        plt.plot(pulse)
        plt.show()
        # plot(pulse)
        # pulse <- pulse/(2*max(abs(pulse))-sum(abs(pulse)))
        # plot(pulse)
        p1 = pulse[round((wl / 2) + 1) : wl]

        IN = [i for i in range(len(pulse)) if i < round((wl / 2) + 1) or i > wl]
        p2 = pulse[:, IN]

        pulse = [p1, p2]
        lgr.debug(pulse)
        fl = wave.frames + len(pulse) - 1
        log_fl = math.log2(fl)
        if log_fl != math.floor(log_fl):
            p = math.ceil(log_fl)
            nzp = 2**p - fl
            wave1 = [np.repeat(0, math.floor(nzp / 2)), wave.data, np.repeat(0, math.ceil(nzp / 2))]

        W = np.hanning(wl=wl)
        plt.plot(W)
        wave2 = np.convolve(wave1[0], pulse * W, mode="valid")
        plt.plot(wave2)
        n2 = len(wave2)
        diffn = n2 - n
        if diffn > 0:
            wave2 = wave2[math.floor(diffn / 2) : (n + math.floor(diffn / 2) - 1)]
        else:
            wave2 = [np.repeat(0, math.floor(abs(diffn / 2)) + 1), wave2, np.repeat(0, math.floor(abs(diffn / 2)))]
        plt.plot(wave2)
        wave2 = wave2 - np.nanmean(wave2)
        if rescale:
            wave2 = rescale(wave2, lower=min(wave), upper=max(wave))

        wave2 = Wave(data=wave2, rate=f)
        if listen:
            listen(wave2, f=f)

        return wave2
    except Exception as msg:
        lg.exception(msg)
