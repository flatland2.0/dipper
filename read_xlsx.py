#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Common procedures to treat CITs"
__contact__ = "framondj@gmail.com"


import datetime
import io
import logging
import math
import os
import sys
import wave
from dataclasses import dataclass
from os.path import dirname
from os.path import join as pjoin

import jflogging
import matplotlib.pyplot as plt
import numpy as np
import openpyxl as xl
import pandas as pd
import regex
import scipy.interpolate as interp
import scipy.io
import sounddevice as sd
from scipy.io import wavfile

# sample rate
f = 44100

lgr = jflogging.Logger(name="R")
lgr.set_debug(True)
lg = logging.getLogger()

# R ncol(df) :Python len(df.columns)
# R nrow(df) :Python len(df)
# R dim(df) :Python df.shape
# R x <- data.frame(X = c(1, 2, 3, 4, 5)) :Python x = pd.DataFrame({X: [1, 2, 3, 4, 5]})


@dataclass
class Wave:
    left: np.array = None  # np.empty((1,), dtype=float)  # None
    right: np.array = None  # np.empty((1,), dtype=float)  # None
    stereo: bool = False
    rate: int = 44100
    bit: int = 16
    pcm: bool = True
    duration: float = 0.0
    frames: int = 0

    def show(self):
        return f"""
Wave Object
\tNumber of Samples:\t{self.frames}
\tDuration (seconds):\t{self.duration}
\tSamplingrate (Hertz):\t{self.rate}
\tChannels (Mono/Stereo):\t{"Stereo" if self.stereo else "Mono"}
\tPCM (integer format):\t{self.pcm}
\tBit (8/16/24/32/64):\t{self.bit}\n
\tleft: {self.left}
\tright: {self.right}\n
"""

    def setValidity(self):
        if not isinstance(self.rate, int):
            raise ValueError("'rate' must be a positive integer")
        if self.bit not in [1, 8, 16, 24, 32, 64]:
            return "'bit' must one of (1, 8, 16, 24, 32 or 64)"
        if not isinstance(self.pcm, bool):
            return "'pcm' must be True or False"
        if self.pcm and self.bit == 64:
            raise ValueError("pcm Waves must have a resolution < 64 bit")
        if not self.pcm and self.bit not in [32, 64]:
            raise ValueError("Float (non pcm) Waves must have a resolution of either 32 or 64 bit")
        if not isinstance(self.stereo, bool):
            return "'stereo' of a Wave must be True or False"
        if self.stereo:
            if len(self.left) != len(self.right):
                raise ValueError("both channels of Waves must have the same length")
        else:
            if len(self.right) > 0:
                raise ValueError("'right' channel of a wave is not supposed to contain data if slot stereo==False")
        return True

    def __add__(self, wave):
        w = self
        if wave.rate != self.rate:
            wave.resample(self.rate)
        w.left = np.concatenate((self.left, wave.left))
        if self.stereo:
            w.right = np.concatenate((self.right, wave.right))
        w.frames = len(w.left)
        w.duration = w.frames / w.rate
        return w

    def resample(self, newrate):
        newframes = int(self.dur * newrate)
        self.rate = newrate
        x_orig = np.linspace(0, self.frames, self.frames)
        x_res = np.linspace(0, newframes, newframes)
        self.left = np.interp(x_res, x_orig, self.left)
        if wave.stereo:
            self.right = np.interp(x_res, x_orig, self.right)
        return self

    def split(self, at=0, units="seconds"):
        match units:
            case "minutes":
                at *= 60
            case "hours":
                at *= 3600
        if at > self.duration:
            raise ValueError("cannot split outside")

        pos = int(self.rate * at if at != 0 else 1)
        left = np.array_split(self.left, [pos])
        if self.stereo:
            right = np.array_split(self.right, [pos])
        self.frames = pos
        self.left = left[0]
        if self.stereo:
            self.right = right[0]
        self.duration = self.frames / self.rate
        w = Wave()
        w.bit = self.bit
        w.stereo = self.stereo
        w.pcm = self.pcm
        w.rate = self.rate
        w.left = left[1]
        if w.stereo:
            w.right = right[1]
        w.frames = len(w.left)
        w.duration = w.frames / w.rate
        return self, w

    def cut(self, from_=None, to_=None, units="seconds"):
        """
        Extract a subset of a Wave
        Args:
            wave: original wave
            from_: starting time. Defaults to None.
            to_: ending time. Defaults to None.
            units: one of seconds, minutes, hours. Defaults to "seconds".
        Returns:
            Wave dataclass
        """
        match units:
            case "minutes":
                u = 60
            case "hours":
                u = 3600
            case _:
                u = 1

        from_frame = int(from_ * u * self.rate) if from_ else 0
        if from_frame >= self.frames:
            return self
        to_frame = int(to_ * u * self.rate) if to_ else self.frames
        if to_frame > self.frames:
            to_frame = self.frames
        self.frames = to_frame - from_frame
        self.left = self.left[from_frame:to_frame]
        if self.stereo:
            self.right = self.right[from_frame:to_frame]
        self.duration = self.frames / self.rate
        return True


#### Functions ###############################################################################
def readWave(rec_name, from_=None, to_=None, units="seconds"):
    """
    simulate the behavior of the tuneR readWave function
    Args:
        rec_name: wave file name
        from_: starting time. Defaults to None.
        to_: ending time. Defaults to None.
        units: one of seconds, minutes, hours. Defaults to "seconds".

    Returns:
        Wave dataclass
    """
    match units:
        case "minutes":
            u = 60
        case "hours":
            u = 3600
        case _:
            u = 1

    with wave.open(rec_name, "rb") as wav:
        params = wav.getparams()
        w = Wave()
        from_frame = int(from_ * u * params.framerate) if from_ else 0
        to_frame = int(to_ * u * params.framerate) if to_ else params.nframes
        if from_frame >= params.nframes:
            from_frame = 0
        if to_frame > params.nframes:
            to_frame = params.nframes
        w.frames = to_frame - from_frame
        w.rate = params.framerate
        w.duration = w.frames / w.rate
        wav.setpos(from_frame)
        wavFrames = wav.readframes(w.frames)
        signal_array = np.array(np.frombuffer(wavFrames, dtype=np.int16), dtype=float)
        if params.nchannels == 2:
            w.stereo = True
            w.left = signal_array[0::2]
            w.right = signal_array[1::2]
        else:
            w.stereo = False
            w.right = np.empty((0,), dtype=float)
            w.left = signal_array
    # lgr.debug(w.show())
    return w


def cutw(wave, from_=None, to_=None, units="seconds"):
    """
    Extract a subset of a Wave
    Args:
        wave: original wave
        from_: starting time. Defaults to None.
        to_: ending time. Defaults to None.
        units: one of seconds, minutes, hours. Defaults to "seconds".
    Returns:
        Wave dataclass
    """
    match units:
        case "minutes":
            u = 60
        case "hours":
            u = 3600
        case _:
            u = 1
    w = wave
    from_frame = int(from_ * u * wave.rate) if from_ else 0
    if from_frame >= wave.frames:
        return wave
    to_frame = int(to_ * u * wave.rate) if to_ else wave.frames
    if to_frame > wave.frames:
        to_frame = wave.frames
    w.frames = to_frame - from_frame
    w.left = wave.left[from_frame:to_frame]
    if wave.stereo:
        w.right = wave.right[from_frame:to_frame]
    w.duration = w.frames / w.rate
    return w


def splitw(wave, at=0, units="seconds"):
    match units:
        case "minutes":
            at *= 60
        case "hours":
            at *= 3600
    if at > wave.duration:
        raise ValueError("cannot split outside")

    pos = int(wave.rate * at if at != 0 else 1)
    left = np.array_split(wave.left, [pos])
    if wave.stereo:
        right = np.array_split(wave.right, [pos])

    wave.frames = pos
    wave.left = left[0]
    if wave.stereo:
        wave.right = right[0]
    wave.duration = wave.frames / wave.rate
    w = Wave()
    w.bit = wave.bit
    w.stereo = wave.stereo
    w.pcm = wave.pcm
    w.rate = wave.rate
    w.left = left[1]
    if w.stereo:
        w.right = right[1]
    w.frames = len(w.left)
    w.duration = w.frames / w.rate
    return wave, w


def resample(wave, threshold=20000):
    """
    Resample a wave
    Args:
        wave: original wave
        threshold: minimum number of datapoints
    Returns:
        Wave dataclass
    """
    res = 2 ^ round(math.log2(round(wave.frames / threshold)))
    w = wave
    w.frames = int(wave.frames / res)
    w.rate = int(w.frames / w.duration)
    x_orig = np.linspace(0, wave.frames, wave.frames)
    x_res = np.linspace(0, w.frames, w.frames)
    w.left = np.interp(x_res, x_orig, wave.left)
    if wave.stereo:
        w.right = np.interp(x_res, x_orig, wave.right)
    return w


def spectro(wave, channel="left", palette=None, title=None, figsize=(10, 10), cmaplimits=None, from_=None, to_=None, units="seconds"):
    if from_ or to_:
        wave = cut(wave, from_=from_, to_=to_, units=units)
    if not palette:
        palette = "viridis"
    # Matplotlib.pyplot.specgram() function to generate spectrogram
    # spectro(rec_name, fastdisp=True, palette=reverse.gray.colors.1,  collevels=cl, flim=c(0, 10))
    d = wave.right if wave.stereo and channel == "right" else wave.left
    if not title:
        title = f"Spectrogram of {channel} Channel".capitalize()

    plt.figure(figsize=figsize)
    plt.title(title)
    plt.xlabel("Time [s]")
    plt.ylabel("Frequency (Hz)")
    plt.xlim(0, wave.duration)
    plt.specgram(d, Fs=f, cmap=palette, vmin=cmaplimits[0] if cmaplimits else None, vmax=cmaplimits[1] if cmaplimits else None)
    plt.colorbar()
    plt.show()


def plot(wave, channel="left", times=None, title=None, figsize=(10, 10), fastdisp=False):
    if fastdisp and wave.frames > 20000:
        wave = resample(wave)
    if not times:
        times = np.linspace(0, wave.duration, num=wave.frames)
    d = wave.right if wave.stereo and channel == "right" else wave.left
    if not title:
        title = f"{channel} Channel".capitalize()
    plt.figure(figsize=figsize)
    plt.title(title)
    plt.ylabel("Signal Value")
    plt.xlabel("Time (s)")
    plt.xlim(0, wave.duration)
    plt.plot(times, d)
    plt.show()


def oscillo(wave, channel="left", times=None, title=None, figsize=(10, 10), fastdisp=False, from_=None, to_=None, units="seconds"):
    if from_ or to_:
        wave = cut(wave, from_=from_, to_=to_, units=units)
    if fastdisp and wave.frames > 20000:
        wave = resample(wave)
    if not times:
        times = np.linspace(0, wave.duration, num=wave.frames)
    d = wave.right if wave.stereo and channel == "right" else wave.left
    if not title:
        title = f"{channel} Channel".capitalize()

    plt.figure(figsize=figsize)
    plt.title(title)
    plt.ylabel("Amplitude")
    plt.xlabel("Time (s)")
    plt.xlim(0, wave.duration)
    plt.plot(times, d)
    plt.show()


def beep(f=1000.0, d=1.0, rate=44100, plot=False, listen=False):
    # generates a beep with a 10% fade in and 10% fade out
    # f = frequency of the beep in Hz
    # d = duration of the beep in seconds
    # based on the package seewave. L. Framond, July 2021.
    w = synth(f, d, rate)
    # lgr.debug(w.show(), w, title="beep1")
    fadedur = 0.1 * d
    w = fade(w, rate, din=fadedur, dout=fadedur)
    if plot:
        oscillo(w)  # , fastdisp=True)
    if listen:
        play(w, f=rate)
    # lgr.debug(w.show(), w, title="beep")
    return w


def fade_channel(wave, ndin=0, ndout=0):
    # wave = wave / max(abs(wave))
    IN = np.linspace(0, 1, num=ndin)
    OUT = np.linspace(1, 0, num=ndout)
    MID = np.full((len(wave) - (len(IN) + len(OUT)),), 1.0)
    FADE = np.concatenate((IN, MID, OUT))
    # w = np.array([wa * fa for wa, fa in zip(wave, FADE)])
    # w = w / max(abs(w))
    # return w
    return np.array([wa * fa for wa, fa in zip(wave, FADE)])


def fade(wave, f=44100, din=0, dout=0):
    ndin = int(din * f)
    ndout = int(dout * f)
    if din == 0 and dout == 0:
        return wave
    if ndin + ndout > wave.frames:
        raise ValueError("The sum of fade in and fade out durations cannot be longer than wave length.")
    if ndin > wave.frames:
        raise ValueError("Fade in duration cannot be longer than wave length.")
    if ndout > wave.frames:
        raise ValueError("Fade in duration cannot be longer than wave length.")

    w = wave
    w.left = fade_channel(wave.left, ndin, ndout)
    if wave.stereo:
        w.right = fade_channel(wave.right, ndin, ndout)
    return w


def synth(f, d, rate):
    n = round(rate * d)
    t = np.array([math.sin(f * x) for x in np.linspace(0, d * 2 * math.pi, num=n)])
    m = max(abs(t))
    t = np.array([(x / m) for x in t])
    return Wave(left=t, right=np.empty((0,), dtype=float), rate=rate, duration=d, frames=n)


def play(sound, f=None, from_=None, to_=None):
    if from_ or to_:
        sound = cut(sound, from_, to_)
    sd.play(sound.left, samplerate=f)
    sd.wait()
    sd.stop()


def interpolate():
    x = np.linspace(0, 2 * np.pi, 10)
    y = np.sin(x)
    x_values = np.linspace(0, 2 * np.pi, 50)
    yinterp = np.interp(x_values, x, y)
    lgr.debug(x, y)
    lgr.debug(x_values, yinterp)
    plt.plot(x, y, "o")
    plt.plot(x_values, yinterp, "-x")
    plt.show()


def normalize(wave, unit="1", center=True, level=1, rescale=True, pcm=None):
    wave.setValidity()
    if not pcm:
        pcm = wave.pcm
    if unit not in ["1", "8", "16", "24", "32", "64"]:
        raise ValueError(
            "'unit' must be either 1 (real valued norm.), 8 (norm. to 8-bit), 16 (norm. to 16-bit), 24 (...), 32 (integer or real valued norm., depends on pcm), or 64 (real valued norm.)"
        )
    if unit == "64":
        pcm = False
    if unit in ["8", "16", "24"]:
        pcm = True
    if center:
        wave.left = wave.left - np.mean(wave.left)
        if wave.stereo:
            wave.right = wave.right - np.mean(wave.right)

    if wave.bit == 8 and all(wave.right >= 0) and all(wave.left >= 0):
        wave.left = wave.left - 127
        if wave.stereo:
            wave.right = wave.right - 127

    if unit != "0":
        if rescale:
            m1 = max(wave.left)
            m = max(m1, max(wave.right)) if wave.stereo else m1
        else:
            if pcm:
                match wave.bit:
                    case 1:
                        m = 1
                    case 8:
                        m = 128
                    case 16:
                        m = 32768
                    case 24:
                        m = 8388608
                    case 32:
                        m = 2147483648
            else:
                m = 1

        if m != 0:
            wave.left = level * wave.left / m
            if wave.stereo:
                wave.right = level * wave.right / m

        if pcm:
            mult = None
            match unit:
                case "8":
                    mult = (127, 127)
                case "16":
                    mult = (32767, 0)
                case "24":
                    mult = (8388607, 0)
                case "32":
                    mult = (2147483647, 0)
            if mult:
                wave.left = np.round(wave.left * mult[0] + mult[1])
                if wave.stereo:
                    wave.right = np.round(wave.right * mult[0] + mult[1])

    wave.bit = 32 if unit == "1" else int(unit)
    wave.pcm = pcm

    return wave


def change_dB(sound, dB):
    # changes the recording's amplitude in dB
    # (to remove or add dBs, no need to calculate the equivalent FS value)
    # sounds =  a "wave" File
    # dB = the number of dB to add (positive value) or remove (negative value)
    # L. Framond, Apr 2021
    m1 = max(sound.left)
    m = max(m1, max(sound.right)) if sound.stereo else m1
    curdB = round(20 * math.log10(m), 1)
    newdB = curdB + dB
    newpercent = 10 ^ int(newdB / 20)
    lgr.debug(m1, m, curdB, newdB, newpercent)
    w = normalize(sound, unit="1", level=newpercent)
    return w


def addsilw(wave, at="end", choose=False, d=None, plot=False, listen=False):
    if at not in ["start", "middle", "end"]:
        raise ValueError("at must be one of start, middle, or end")
    if not d:
        raise ValueError("silence duration has to be set with the argument 'd'")
    sil = np.zeros((int(d * wave.rate),))
    match at:
        case "start":
            at = 0
        case "middle":
            at = wave.frames / (2 * wave.rate)
        case "end":
            at = wave.frames / wave.rate

    if choose:
        print("choose position on the wave\n")
        oscillo(wave, f=f)
        # coord=locator(n=1)
        # at=coord[x[1]]
        # abline(v=at,col=2,lty=2)

    pos = int(at * wave.rate)
    w = wave
    sil = np.zeros((int(d * wave.rate),))
    w.left = np.concatenate((wave.left[0:pos], sil, wave.left[pos + 1 :]))
    if wave.stereo:
        w.right = np.concatenate((wave.right[0:pos], sil, wave.right[pos + 1 :]))
    w.frames = len(w.left)
    w.duration = w.frames / w.rate
    w.left = np.concatenate((wave.left[0:pos], sil, wave.left[pos + 1 :]))
    if wave.stereo:
        w.right = np.concatenate((wave.right[0:pos], sil, wave.right[pos + 1 :]))
    w.frames = len(w.left)
    w.duration = w.frames / w.rate
    if plot:
        oscillo(wave=w)
    if listen:
        play(w)
    return w


def addsil(wave, at="end", choose=False, d=None, plot=False, listen=False):
    if at not in ["start", "middle", "end"]:
        raise ValueError("at must be one of start, middle, or end")
    if not d:
        raise ValueError("silence duration has to be set with the argument 'd'")
    sil = Wave()
    sil.bit = wave.bit
    sil.stereo = wave.stereo
    sil.rate = wave.rate
    sil.left = np.zeros((int(d * wave.rate),))
    if sil.stereo:
        sil.right = sil.left
    sil.frames = len(sil.left)
    sil.duration = d
    match at:
        case "start":
            return sil + wave
        case "middle":
            at = wave.frames / (2 * wave.rate)
        case "end":
            return wave + sil

    if choose:
        print("choose position on the wave\n")
        oscillo(wave, f=f)
        # coord=locator(n=1)
        # at=coord[x[1]]
        # abline(v=at,col=2,lty=2)

    w1, w2 = splitw(wave, at=at)

    lgr.debug(sil.show(), w1.show())
    w = w1 + sil + w2

    if plot:
        oscillo(wave=w)
    if listen:
        play(w)
    return w


def noisew(f, d, type="unif", stereo=False):
    noise = np.random.normal(int(d * f)) if type == "gaussian" else np.random.uniform(size=int(d * f), low=-1, high=1)
    w = Wave(rate=f, duration=d, left=noise, right=np.empty((0,)), frames=len(noise), stereo=stereo)
    if stereo:
        w.right = noise
    return w


def make_calib_pb(freqs, d, SNR=0):
    # generates a playbacks of beeps for calibration
    # freqs = a vector of frequencies to use
    # d = duration of the beeps
    # SNR = if the playback should have some noise in the background, choose the SNR here. default = 0
    # based on package seewave. L. Framond, June 2021.
    beeps = [beep(f=freq, d=d) for freq in freqs]
    for i, b in enumerate(beeps):
        if SNR != 0:
            noise = noisew(f=44100, d=b.duration)
            noise = fade(noise, din=0.1, dout=0.1)
            noise = change_dB(noise, dB=(-1 * SNR))
            b.left += noise.left
            if b.stereo:
                b.right += noise.left
        b = addsilw(b, at="start", d=d if i == 0 else d * 2)

    beeps[-1] = addsilw(beeps[-1], at="end", d=d)
    return concatenatew(beeps)


def concatenatew(waves):
    w = Wave()
    left = [wave.left for wave in waves]
    right = [wave.right for wave in waves]
    w.left = np.concatenate((left))
    w.right = np.concatenate((right))
    w.frames = len(w.left)
    w.rate = waves[0].rate
    w.stereo = waves[0].stereo
    w.duration = w.frames / w.rate
    return w


################################
def main():
    try:

        wl = 512
        # freq = np.linspace(start=0, stop=f / 2 - f / wl, num=int(wl / 2))
        # from_ = 250
        # # x = np.min(abs(freq - from_))
        # x = np.min(abs(freq))
        # lgr.debug(freq, x)

        # Import the excel file and call it xls_file
        # excel_file = pd.ExcelFile("./WAV/Metadata (1).xlsx")
        # View the excel_file's sheet names
        # lgr.debug(excel_file.sheet_names)

        # Load the excel_file's Sheet1 as a dataframe
        # meta = excel_file.parse("Sheet1")
        # lgr.debug(meta)

        # using dictionary to convert specific columns
        # convert_dict = {
        #     "Dist_bird_m": int,
        #     "Ampl_calib_dBA": int,
        #     "Sec_start_calib": int,
        # }
        # meta = meta.astype(convert_dict)
        # lgr.debug(meta.Sec_start_calib)  # = int(meta['Dist_bird_m'])

        # files = meta.loc[meta["Sec_start_calib"] >= 0, "Sec_start_calib"]
        # print(files)
        rec_name = "./WAV/2023-01-17-001/MZ000011.WAV"
        w = readWave(rec_name, from_=2.0, to_=12.5)
        # oscillo(w)
        # w5 = noisew(f=44100, d=5)
        # oscillo(w5)
        # play(w5)

        w1 = beep(1000.0, 1.0)  # , plot=True, listen=True)
        w6 = beep(2000.0, 1.0)  # , plot=True, listen=True)
        w7 = w1 + w6
        # oscillo(w7)
        # play(w7)
        w8, w9 = w7.split(at=1.5)
        oscillo(w8, title="W7-1")
        oscillo(w9, title="w7-2")
        # lgr.debug(w1.show(), w1.setValidity(), w1, title="w1")
        oscillo(w1, title="W1")
        w10 = addsil(w1, at="middle", d=1, plot=True, listen=True)
        oscillo(w10)
        w11 = addsil(w1, at="end", d=1.5, plot=True, listen=True)
        oscillo(w11)
        w4 = make_calib_pb([1000, 1500, 3000], d=0.5, SNR=4)
        oscillo(w4)
        play(w4)
        w2 = cutw(w, to_=0.1)
        oscillo(w2)
        # oscillo(w, fastdisp=True)
        # fade(w2, din=0.02, dout=0.02)  # , plot=True, listen=True)
        # lgr.debug(w2)
        w3 = change_dB(w, -6)
        oscillo(w3)

        # synth(1000.0, 5.0, 44100, plot=True, listen=True)

        # spectro(w, palette="Greys", figsize=(10, 10), title="dipper", cmaplimits=(-20, 50))
        # oscillo(w, figsize=(15, 5), fastdisp=True, from_=2, to_=2.5)

        # time = np.linspace(0.0, length, data.shape[0])
        # time = np.linspace(0.0, 250, len(w.left))

        # plt.plot(time, data[:, 0], label="Left channel")
        # times = np.linspace(0, w.duration, num=w.frames)
        # plt.figure(figsize=(15, 5))
        # plt.plot(times, w.left)
        # plt.title("Left Channel")
        # plt.ylabel("Signal Value")
        # plt.xlabel("Time (s)")
        # plt.xlim(0, w.duration)
        # plt.show()

        # plt.plot(time, w, label="Left channel")
        # plt.legend()
        # plt.xlabel("Time [s]")
        # plt.ylabel("Amplitude")
        # plt.show()

        # raise ValueError("aaa")
    except Exception as msg:
        lg.exception(msg)
        lgr.info("something went wrong: %s" % msg)
        exit(0)


#### Run main ##########################################################
if __name__ == "__main__":
    main()
