import pandas as pd
import matplotlib as plt
import numpy as np
import math
import wave
### SETTINGS  ###

# sample rate
f=44100

# buffer time before and after a call was detected
buf = 2250

# spectrogram window size
ws = 256

# smoothing of envelope 
dur_smooth = 0.005
sm = f*dur_smooth



def start_and_end(starts_spl, ends_spl):
    #function that calculates the starts and ends of calls based on the results of comparing the threshold with the envelope

    while starts_spl[1] > ends_spl[1]:
        ends_spl = ends_spl[-1]

    if len(ends_spl) < len(starts_spl):
        ends_spl[(len(ends_spl)+1):len(starts_spl)] = len(rec_filt)

    for j in range(1,len(starts_spl)):
        if starts_spl[j] > buf:
            starts_spl[j] = starts_spl[j] - buf
        else:
            starts_spl[j] = 1

        if ends_spl[j] > len(rec_filt) - buf:
            ends_spl[j] = len(rec_filt)
        else:
            ends_spl[j] = ends_spl[j] + buf

    while starts_spl[1]>ends_spl[1]:
            ends_spl = ends_spl[-1]

    call_limits = data.frame(starts_spl, ends_spl)

    return(call_limits)

##################################################################################

def detect_calls(THR, env_rec):
    # function to calculate the start and end of calls within a recording
    
    thr = THR*max(env_rec)
    
    # plot the detection threshold
    plot(env_rec, type = "l")
    abline(h=thr)
    
    # convert between the envelope and the samples
    multip = len(rec_filt@left)/len(env_rec)
    # detect all songs 
    env_thr = [e for e in env_rec if e - thr > 0] #diff(env_rec-thr>0)
    ends_spl = [e * multip for e in env_thr if e == -1] #which(env_thr==-1)
    starts_spl = [e * multip for e in env_thr if e == 1] #which(env_thr==1)

    call_lims = start_and_end(starts_spl, ends_spl)
    return call_lims
  

################################################################################
def pert(nthr):
        
    # env_start = mid_env-which(dB(env_call[mid_env:1], ref= 1) < dB_thr[nthr])[1]
    env_start = mid_env - next([d for d in dB(env_call[mid_env:1], ref= 1) if d < dB_thr[nthr]])
    # env_end = which(dB(env_call[mid_env:len(env_call)], ref=1) < dB_thr[nthr])[1]
    env_end = next([d for d in dB(env_call[mid_env:len(env_call)], ref=1) if d < dB_thr[nthr]])

    if env_start.isnull:
        env_start = 1

    if env_end.isnull:
        env_end = len(env_call)
    else:
        env_end = env_end + mid_env
    
    spl_start = round(env_start*multip)
    
    if spl_start<1:
        spl_start=1
    
    spl_end = round((env_end)*multip)
    
    if spl_end>len(call):
        spl_end=len(call)
    
    
    # print(c(spl_start, spl_end))
    call_dur = (spl_end - spl_start)/f
    
    call_time = (timestamps[x,1] + spl_start)/f + start
    call_time_win = spl_start/f
    
    # print(call_dur)    
    if call_dur<0.02:
        call_extracted = call # this only for cases where call detection fails so that the code doesn't fail entirely
    else:
        # extract call based on the 12 dB below peak thr
        call_extracted = call[spl_start:spl_end]
    
    par(mfrow=[1,1])
    # spectro(call_extracted, fastdisp = T, f=f, palette=reverse.gray.colors.1, collevels=seq(-50,0,5), wl = ws, ovlp = 90)
    
    # amplitudes
    pe_dB_FS = dB(max(call_extracted), ref = (2^15)-2)
    rms_dB_FS = RMS_dB(call_extracted)
    
    # SNR between the call defined with the 12 dB THR and the part following the call immediately
    SNR = rms_dB_FS - RMS_dB(call[c(1:spl_start, spl_end:len(call))])
    
    # freq mesaurements
    par(mfrow=[1,1])
    spectrum = meanspec(call_extracted, f = f, ovlp = 99)
    max_frq = which.max(spectrum[,2])
    Fp_kHz = spectrum[max_frq,1]
    lof = [d for d in dB(spectrum[max_frq:1,2], ref= 1) if d < dB_thr[nthr]]
    lof_index = lof[1]
    loF_kHz = spectrum[max_frq-lof_index,1]
    hif = [d for d in dB(spectrum[max_frq:len(spectrum[,2]),2], ref = 1) if d < dB_thr[nthr]]
    hif_index = hif[1]
    
    hiF_kHz = spectrum[max_frq+hif_index,1]
    bw = hiF_kHz-loF_kHz
    plot(spectrum, type = "l")
    abline(h=undB(dB_thr, ref = 1))
    
    # freq measurements at timepoints
    spectrum = meanspec(call_extracted[1:512], f = f,ovlp = 99,plot = F)
    max_frq = which.max(spectrum[,2])
    Fstart_kHz = spectrum[max_frq,1]
    
    spectrum = meanspec(call_extracted[(len(call_extracted)-512):len(call_extracted)], f = f, ovlp = 99,plot = F)
    max_frq = which.max(spectrum[,2])
    Fend_kHz = spectrum[max_frq,1]
    
    around_peak = c(max(c((which.max(call_extracted)-256), 1)): min(c((which.max(call_extracted)+256), len(call_extracted))))
    if len(around_peak)<512:
        if around_peak[1]==1:
            around_peak = 1:512
        else:
            around_peak = (len(call_extracted)-512):len(call_extracted)
    
    spectrum = meanspec(call_extracted[around_peak], f = f, ovlp = 99, plot = F)
    max_frq = which.max(spectrum[,2])
    Fmax_ampl_kHz = spectrum[max_frq,1]

    # collect data
    call_pars = data.frame(
        dB_thr[nthr],
        call_time,
        call_time_win,
        call_dur, 
        Fp_kHz,
        loF_kHz,
        hiF_kHz,
        bw,
        Fstart_kHz,
        Fend_kHz,
        Fmax_ampl_kHz,
        pe_dB_FS,
        rms_dB_FS,
        SNR
    )
    return call_pars
        
######################
def callp(x):
        
    print(paste0("     ...analysing call ", x))
    
    # snip call out
    call =  rec_filt@left[timestamps[x,1]:timestamps[x,2]]
    # X11()
    # spectro(call, f=f, fastdisp = T, palette=reverse.gray.colors.1, collevels=seq(-50,0,5), wl = ws, ovlp = 90)
    
    # calculate envelope
    par(mfrow=[1,1])
    env_call = env(call, msmooth = [sm,50], fastdisp = True, f=f, norm = True)
    # plot(env_call, type = "l") 
    # abline(h=undB(dB_thr, ref = 1))
    # convert between the envelope and the samples
    multip = len(call)/len(env_call)
    
    # call start, end, dur
    mid_env = round(len(env_call)/2)
    
    perthr = list(map(pert(nthr),nanalys)) #lapply(1:nanalys, pert(nthr))
    
    # names_cols = paste0(
    #     rep(c("dB_thr",
    #     "call_time",
    #     "call_time_win",
    #     "call_dur", 
    #     "Fp_kHz",
    #     "loF_kHz",
    #     "hiF_kHz",
    #     "bw",
    #     "Fstart_kHz",
    #     "Fend_kHz",
    #     "Fmax_ampl_kHz",
    #     "pe_dB_FS",
    #     "rms_dB_FS",
    #     "SNR"), 
    #     times = nanalys),
    #     "_",
    #     rep(-1*dB_thr, each = 14), "dB"
    #     )
    names_cols = paste0(
        rep(
            [
                "dB_thr",
                "call_time",
                "call_time_win",
                "call_dur", 
                "Fp_kHz",
                "loF_kHz",
                "hiF_kHz",
                "bw",
                "Fstart_kHz",
                "Fend_kHz",
                "Fmax_ampl_kHz",
                "pe_dB_FS",
                "rms_dB_FS",
                "SNR"
            ], 
        times = nanalys
        ),
        "_",
        rep(-1*dB_thr, each = 14), "dB"
        )
    # perthr2 = do.call(cbind, perthr)
    perthr2 = do.call(pd.concat(perthr))
    colnames(perthr2) = names_cols
    
    return perthr2

        
#######################
def analyse_calls(rec_filt, timestamps, dB_thr = [-12,-6]):
  
    #function that performs the call analysis from all calls 
    
    ncalls = len(timestamps[,1])
    x=4
    nanalys = len(dB_thr)
    
    call_params = list(map(callp(x), ncalls)) # lapply(1:ncalls, callp(x))
        
    # all_calls = do.call(rbind, call_params)
    
    all_calls = do.call(pd.concat(call_params))
    return all_calls



##########################################################################

def ggspectro(sound, col = ["A", "grey"], ymin = .1, ymax = 12, ovlp = 90, wl = 256, f=44100):
  
    s = spectro(sound, ovlp = ovlp, wl = ws, plot = F, f=f)
    spec = melt(s['amp'])
    spec['Time'] = rep(s'time'], each = len(s['freq']))
    spec['Frequency'] = rep(s['freq'], times = len(s['time']))
    rm(s)
    spec['value3'] = -0.5 if spec['value']==0 else spec['value']
    spec['value2'] = math.log(-spec['value3'])
    
    if len(col)>1:
        col = col[1]
    
    if col != "grey":
        p = ggplot(data = spec, aes(x = Time, y = Frequency))+
            geom_raster(aes(fill = value2))+
            scale_fill_viridis_c("Amplitude",option = col, direction = -1, guide = "none")+
            theme_classic()+
            scale_y_continuous(
                limits = [ymin, ymax], 
                expand = [0,0], 
                breaks = 1:12) +
                scale_x_continuous(expand = [0,0]
                )+
            geom_hline(yintercept = 1:12, lty = 2, col = "grey")

    else:
        p = ggplot(data = spec, aes(x = Time, y = Frequency))+
            geom_raster(aes(fill = value2))+
            scale_fill_gradient("Amplitude",high = "white", low = "black",guide="none" )+
            theme_classic()+
            scale_y_continuous(limits = [ymin, ymax], expand = [0,0])+
            scale_x_continuous(expand = [0,0])+
            geom_hline(yintercept = 1:12, lty = 2, col = "grey")
        
    return p

###########################################################################
def dt(i):
    indices = (1+(i-1)*14):((i)*14)
        
    conv = int(vals[indices])
    convstart = conv[3] 
    convend = conv[3] + conv[4]
        
    dd = data.frame(x = [convstart, convend], y = [conv[9], conv[10]])
        
    dat =[conv, convstart, convend]
    return dat

###################################
def plot_call_analysis(call, call_params, call_id):
    thrs = [-12,-6] 
    vals = call_params[call_id,]
    # conv$start6 = conv$"call_time_-6dB"-call_timestamps[call_id,1]/f-start
    # conv$end6 = conv$"call_time_-6dB" + conv$"call_dur_-6dB" - call_timestamps[call_id,1]/f-start
    # conv$start12 = conv$"call_time_-12dB"-call_timestamps[call_id,1]/f-start
    # conv$end12 = conv$"call_time_-12dB" + conv$"call_dur_-12dB" - call_timestamps[call_id,1]/f-start
    
    p = ggspectro(call)
    
    n_lines = len(thrs)
    i=1

    dt = t(sapply(1:n_lines, dt(i)))
    
    title = paste0("syllable time : ", round(dt[3],3))
    
    dd = data.frame(
        x = [dt[,15], dt[,16]], 
        y = [dt[,9], dt[,10]], 
        shp = rep(c(1,15), times = n_lines)
    )
    
    p = p +       
        geom_vline(xintercept = dt[,15], linewidth = 1, col = "deepskyblue", lty = [1,2])+
        geom_vline(xintercept = dt[,16], linewidth = 1, col = "deepskyblue", lty = [1,2])+
        geom_hline(yintercept = dt[,5], linewidth = 2, col = "grey80", lty = 1,2])+
        geom_hline(yintercept = dt[,6],  linewidth = 1, col = "green", lty = [1,2])+
        geom_hline(yintercept = dt[,7], linewidth = 1, col = "chartreuse2", lty = [1,2])+
        geom_point(inherit.aes = F, data = dd, aes(x=x,y=y, pch = as.factor(shp)), size = 4, col = "green")+
        ggtitle(title)+
        theme(legend.position = "none")
    
    return p

