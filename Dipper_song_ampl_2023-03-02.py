#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Léna de Framond/Jean de Framond"
__copyright__ = "Copyright 2021-2025 Léna de Framond"
__license__ = "MIT"
__description__ = "Analysis of dipper's songs"
__contact__ = "lena.de.framond@laposte.net"

import pandas as pd
import jflogging
import importlib

lgr = jflogging.Logger(name="Dipper")
lgr.set_debug(True)


def select(options, question):
    df = isinstance(options, pd.DataFrame)
    opts = options[0].values if df else options.values
    txt = options[1].values if df else None
    done = False
    while not done:
        print("\n".join([f"{o}: {txt[i] if df else ''}" for i, o in enumerate(opts)]))
        if (response := input(f"{question} (Q to quit): ")).upper()[0] == "Q":
            exit(0)
        if response not in opts:
            print("\n\nPlease select from the list of choices")
        else:
            done = True
    return response


#### Main program ###############################################################################
def main():
    try:

        # ---- read meta data file --------------------------------------------------------------
        excel_file = pd.ExcelFile("./WAV/Metadata (1).xlsx")

        # Load the excel_file's Sheet1 as a dataframe
        meta = excel_file.parse("Sheet1")
        # use dictionary to convert specific columns
        convert_dict = {
            "Dist_bird_m": int,
            "Ampl_calib_dBA": int,
            "Sec_start_calib": int,
        }
        meta = meta.astype(convert_dict)

        # ----- initialize ----------------------------------------------------------------------
        files = meta.loc[meta["Sec_start_calib"] >= 0, "Sec_start_calib"]
        meta.dB_FS_rms_calib = 0

        # ---- choose session / recording to analyse --------------------------------------------
        session = select(meta.Session, "Please enter the session name")
        lgr.debug(session, title="session")

        newmeta = meta[meta.Session == session]
        lgr.debug(newmeta)

        # ---- then choose action ---------------------------------------------------------------
        choices = pd.DataFrame(
            [["1", "call detection and analysis"], ["2", "check call analysis"], ["3", "calculate source levels"], ["4", "re-reun the analysis"]]
        )
        lgr.debug(choices)
        action = select(choices, "What action do you want to do?")
        lgr.debug(action)

        match action:
            case 1:
                all_call_pars = importlib.import_module(all_call_pars)
                all_call_pars.all_call_pars(session, newmeta)

            case 2:
                call_check = importlib.import_module(call_check)
                call_check.call_check(session, newmeta)

            case 3:
                calc_ampl = importlib.import_module(calc_ampl)
                calc_ampl.calc_ampl(session, newmeta)

            case 4:
                re_run = importlib.import_module(re_run)
                re_run.re_run(session, newmeta)

    except Exception as msg:
        lgr.debug("something went wrong: %s" % msg)
        exit(0)


#### Run main ######################################################################################
if __name__ == "__main__":
    main()
