#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Léna de Framond/Jean de Framond"
__copyright__ = "Copyright 2021-2025 Léna de Framond"
__license__ = "MIT"
__description__ = "Analysis of dipper songs"
__contact__ = "lena.de.framond@laposte.net"

import math

# library(dplyr)
# library(reshape2)
# pip install pandas
import pandas as pd
import numpy as np
from lenawave import *
from lenafunc import *

import call_analysis

# pip install ggplot
import ggplot
import matplotlib as plt
import math

# library(seewave)
# library(tuneR)
import wave

### SETTINGS  ###########################################################################
# sample rate
f=44100

# buffer time before and after a call was detected (in SAMPLES)
buf = 2250

# spectrogram window size (SAMPLES)
ws = 256

# smoothing of envelope (in SECONDS)
dur_smooth = 0.005
sm = f*dur_smooth

# dB threshold to define call duration and frequency max and min
ana_thr = [-12,-6]

# list of logs
logcoldBs = [math.log(x)*15-math.log(20)*15 for x in range(1,20)]

i=1

##################################################
def all_call_pars(session, meta):
    columns=['recnb','Recording_nb','sess','filt']
    all_call_pars=pd.DataFrame(columns=columns)

    for m in meta:
        # read the recording + print spectrogram  
        rec_name = f"./WAV/{m['Session']}/{m['Recording_nb']}.WAV"
        rec = Wave.read(rec_name, from_=m['sec_rec_start'], to_=m['sec_rec_end'], units="seconds")
        
        # X11() # start graphic driver
        # spectro(rec, fastdisp = T)
        cl = np.linspace(start=-60, stop=0, num=15)
        rec.spectro(palette="Greys", collevels=cl, flim=[0, 10])
 
        # choose appropriate High pass filter
        HP = float(input(prompt = "please enter the frequency (Hz) of the high-pass_filter: "))
        if not start:
            start = 0
        # choose a few seconds good for analysis (not too much noise etc)
        start = int(input(prompt = "please enter the timestamp (sec) to start the analysis: "))

        end = int(input(prompt = "please enter the timestamp (sec) to end the analysis: "))
        if not end:
            end = len(rec)/f

        # select just this part
        rec_chup = Wave.read(rec_name, from_=f*start, to_=f*end, units = "seconds")

        # correct for distance-dependent atmospheric attenuation and filter rec
        AA_frqs = np.linspace(start=0, stop=f/2, num=256)
        AA_dBs = f_aa(AA_frqs, m["Tp"], m["RH"]*m["Dist_bird_m"])
        AA_chup = AA_dBs if AA_frqs>HP and AA_frqs<15000 else -60
        AA_filt = undB(AA_chup, ref = 1)
        rec_filt = firLena(rec_chup, custom = AA_filt, output = "Wave", f=f)
        
        par(mfrow=[1,1])
        rec_filt.oscillo(fastdisp = True)
        par(mfrow=[1,1])
        
        #   spectro(rec_filt@left[1:lastspl], f=f, fastdisp = T, palette=reverse.gray.colors.1, 
        #           collevels = seq(-60, 0, length.out = 15), flim = c(0, 10))
        spectrow(rec_filt@left[1:lastspl], f=f, palette="reverse.gray.colors.1", 
          collevels=cl, flim = [0, 10])
  

        #  calculate/plot an envelope
        par(mfrow=[1,1])
        env_rec = env(rec_filt, msmooth = [441,50], plot=F, f=f, norm=True)
        plt.plot((1:length(env_rec))/220, env_rec, type = "l", ylim = c(0,1))
        abline(h = [.1,.2,.3,.4,.5], lty = 2)
  

        # choose a detection threshold
        THR = float(input(prompt = "please enter a threshold (between 0 and 1): "))
        abline(h=THR)

        
        # find starts and ends of calls
        call_timestamps = call_analysis.detect_calls(THR, env_rec, rec_filt)
        print(call_timestamps)
        # call analysis
        # X11()
        call_params = call_analysis.analyse_calls(rec_filt, call_timestamps, dB_thr = ana_thr, start = start)
        call_params['recnb'] = m['Recording_nb']
        call_params['sess'] = m['Session']
        call_params['filt'] = HP
        call_params = cbind(call_params, call_timestamps+start*f)
        st = pd.concat([pd.DataFrame(call_params, columns=all_call_pars.columns), all_call_pars], ignore_index=True)

    # save results
    filename = f"../call_analysis/to_check_{session}.csv"
    if all_call_pars.to_csv(filename, encoding='utf-8', index=False, header=False):
        print("file saved!")
        
        