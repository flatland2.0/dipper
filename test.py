#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Jean de Framond"
__copyright__ = "Copyright 2021-2025 Jean de Framond"
__license__ = "MIT"
__description__ = "Common procedures to treat CITs"
__contact__ = "framondj@gmail.com"

# import datetime
# import io
import logging

# import os
# import sys
import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sounddevice as sd
from enum import Enum
import jflogging
import itertools
from scipy.stats import binomtest
from scipy.fft import fft, ifft
from scipy import signal

# import lenawave as lw
from lenawave import *

# import lenafunc as lf
from lenafunc import *

# import openpyxl as xl
# import regex
# import scipy.interpolate as interp
# from scipy.io import wavfile
# from dataclasses import dataclass
# from os.path import dirname
# from os.path import join as pjoin
# import scipy.io

lgr = jflogging.Logger(name="R")
lgr.set_debug(True)
lg = logging.getLogger()

# R ncol(df) :Python len(df.columns)
# R nrow(df) :Python len(df)
# R dim(df) :Python df.shape
# R x <- data.frame(X = c(1, 2, 3, 4, 5)) :Python x = pd.DataFrame({X: [1, 2, 3, 4, 5]})


def phyloglm(formula=None):  # trait01 ~ predictor, data=dat, phy=tre,  boot=100):
    """
    Return
    phyloglm model:
    coefficients    # the named vector of coefficients.
    alpha 	        # (logistic regression only) the phylogenetic correlation parameter.
    scale 	        # (Poisson regression only) the scale parameter which estimates the overdispersion.
    sd 	            # standard deviation for the regression coefficients.
    vcov 	        # covariance matrix for the regression coefficients.
    logLik 	        # (logistic regression only) log likelihood.
    aic 	        # (logistic regression only) AIC.
    penlogLik 	    # (logistic regression only) penalized log likelihood, using Firth's penalty for coefficients.
    y 	            # response.
    n 	            # number of observations (tips in the tree).
    d 	            # number of dependent variables.
    formula 	    # the model formula.
    call 	        # the original call to the function.
    method 	        # the estimation method.
    convergence 	# An integer code with '0' for successful optimization. With logistic_MPLE, this is the convergence code from the optim routine.
    alphaWarn 	    # (logistic regression only) An interger code with '0' for the estimate of alpha is not near the lower and upper bounds, code with '1' for the estimate of alpha near the lower bound, code with '2' for the estimate of alpha near the upper bound.
    X 	            # a design matrix with one row for each tip in the phylogenetic tree.
    bootmean 	    # (boot > 0 only) bootstrap means of the parameters estimated.
    bootsd 	        # (boot > 0 only) bootstrap standard deviations of the estimated parameters.
    bootconfint95 	# (boot > 0 only) bootstrap 95% confidence interval.
    bootmeanAlog 	# (boot > 0 only) bootstrap mean of the logs of the estimated alphas.
    bootsdAlog 	    # (boot > 0 only) bootstrap standard deviation of the logs of the estimated alphas.
    bootnumFailed 	# (boot > 0 only) number of independent bootstrap replicates for which phyloglm failed. These failures may be due to the bootstrap data having too few 0's or too few 1's.
    bootstrap 	    # (boot > 0 and full.matrix = TRUE only) matrix of all bootstrap estimates.
    """

    model = {
        "Call": "phyloglm(formula=trait01 ~ predictor, data=dat, phy=tre,  boot=100)",
        "AIC": 48.54,
        "logLik": -21.27,
        "Pen.logLik": -19.61,
        "Method": "logistic_MPLE",
        "coefficients": {
            "t": ["Estimate", "StdErr", "z.value", "lowerbootCI", "upperbootCI", "p.value"],
            "(Intercept)": [-0.50567, 0.41742, -1.21140, -1.50615, 0.4158, 0.225740],
            "hab_scores": [1.41266, 0.45578, 3.09946, 0.56413, 2.3718, 0.001939],
        },
        "alpha": 1.241056,
        "scale": 0,
        "sd": 0,
        "vcov": 0,
        "y": 0,
        "n": 0,
        "d": 0,
        "formula": formula,
        "convergence": 0,
        "alphaWarn": 0,
        "X": 0,
        "bootmean": 0,
        "bootsd": 0,
        "bootconfint95": 0,
        "bootmeanAlog": 0,
        "bootsdAlog": 0,
        "bootnumFailed": 0,
        "bootstrap": 0,
    }
    return model


def seq(start, stop, step):
    return np.arange(start, stop + step, step)


def plogis(x):
    """
    Cumulative Logistic Density
    Args:
        x (np.array): vector to work on
    Returns:
        np.array
    """
    if not isinstance(x, np.ndarray):
        raise ValueError("parameter must be a vector")
    return np.divide(1, 1 + np.exp(-x))


def confint(mod):
    binomtest(13, 100).proportion_ci()
    return np.array()


def ribon2(mod, dat, predic):
    """
    function that calculates and gives values for plotting the fitted line and 95% interval of the
    phyloglm model, using the result of boostraping
    works with phyloglm results
    # L. Framond, 03.2021
    Args:
        mod (pd.DataFrame): results of the model
        dat (pd.DataFrame): dataset
        predic (string): character string identifying the predictor variable
    Returns:
        pandas DataFrame: a big dataframe that can be plotted in ggplot2
    """
    # mod = mass_phy
    # dat = mass_dat
    # predic = "hab_scores"

    CI95 = confint(mod)
    CI95 = [
        mod["bootconfint95"][
            1,
        ],
        mod["bootconfint95"][
            2,
        ],
    ]

    colnames_CI95 = ["2.5 %", "97.5 %"]
    beta = mod.coefficients

    predic_dat = predic
    if predic == "log(g_mean_dunning)":
        predic_dat = "g_mean_dunning"
    dat[predic_dat] = np.log(dat[predic_dat])

    xstart = min(dat[predic_dat])
    xend = max(dat[predic_dat])

    Xs = np.linspace(start=xstart, stop=xend, num=50)
    avrg = plogis(beta["(Intercept)"] + beta[predic] * Xs)

    low = []
    hig = []

    for j in range(1, 50):
        low[j] = np.min(
            [
                plogis(beta["(Intercept)"] + beta[predic] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
            ]
        )

        hig[j] = np.max(
            [
                plogis(beta["(Intercept)"] + beta[predic] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(beta["(Intercept)"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "2.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + beta[predic] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "2.5 %"] * Xs[j]),
                plogis(CI95["(Intercept)", "97.5 %"] + CI95[predic, "97.5 %"] * Xs[j]),
            ]
        )

    colnames = ["X", "avrg", "low", "high"]
    ribon = pd.DataFrame.from_dict(dict(zip(colnames, avrg, low, hig)))
    return ribon


def firLena(wave, f, channel=1, from_=None, to_=None, bandpass=True, custom=None, wl=512, wn="hanning", rescale=False, listen=False, output="matrix"):

    # wave = rec
    # # custom = unfilter
    # wl=512
    # wn="hanning"
    # from = 1800
    #
    try:
        # wave = input["w"]
        # bit = wave["bit"]

        freq = np.linspace(start=0, stop=(f / 2) - (f / wl), num=round(wl / 2))
        lgr.debug(round(wl / 2), (f / 2) - (f / wl), freq)
        from_ = round(np.min(abs(freq - from_))) if from_ else 1
        to_ = round(np.min(abs(freq - to_))) if to_ else round(wl / 2)
        lgr.debug(from_, round(np.min(abs(freq - from_))), to_, round(np.min(abs(freq - to_))))
        n = wave.frames
        if custom:
            if custom.shape[1] == 2:
                custom = custom[1]
            if len(custom) != wl / 2:
                raise ValueError("custom filter length has to be equal to 'wl'/2")
            if bandpass:
                filtspec = [custom, custom[::-1]]
            else:
                filtspec = 1 - [custom, custom[::-1]]

        else:
            filtspec = np.repeat(1, wl / 2)
            if bandpass:
                fl = filtspec
                # IN = [i for i in range(len(filtspec)) if i < from_ or i > to_]
                # fl[:, IN] = 0

                filtspec[:from_] = 0
                filtspec[to_:] = 0
                lgr.debug(fl, filtspec)
            else:
                filtspec[from_:to_] = 0

            filtspec = [filtspec, filtspec[::-1]]
        fft = ifft(filtspec) / len(filtspec)
        lgr.debug(fft)
        pulse = fft.real
        lgr.debug(pulse)

        plt.plot(pulse)
        plt.show()
        # plot(pulse)
        # pulse <- pulse/(2*max(abs(pulse))-sum(abs(pulse)))
        # plot(pulse)
        p1 = pulse[round((wl / 2) + 1) : wl]

        IN = [i for i in range(len(pulse)) if i < round((wl / 2) + 1) or i > wl]
        p2 = pulse[:, IN]

        pulse = [p1, p2]
        lgr.debug(pulse)
        fl = wave.frames + len(pulse) - 1
        log_fl = math.log2(fl)
        if log_fl != math.floor(log_fl):
            p = math.ceil(log_fl)
            nzp = 2**p - fl
            wave1 = [np.repeat(0, math.floor(nzp / 2)), wave.sound, np.repeat(0, math.ceil(nzp / 2))]

        W = np.hanning(wl)  # =wl)
        lgr.debug(W, title="W")
        plt.plot(W)
        wave2 = np.convolve(wave1[0], pulse * W, mode="valid", dtype=object)
        plt.plot(wave2)
        n2 = len(wave2)
        diffn = n2 - n
        if diffn > 0:
            wave2 = wave2[math.floor(diffn / 2) : (n + math.floor(diffn / 2) - 1)]
        else:
            wave2 = [np.repeat(0, math.floor(abs(diffn / 2)) + 1), wave2, np.repeat(0, math.floor(abs(diffn / 2)))]
        plt.plot(wave2)
        wave2 = wave2 - np.nanmean(wave2)
        if rescale:
            wave2 = rescale(wave2, lower=min(wave), upper=max(wave))

        wave2 = Wave(sound=wave2, rate=f)
        if listen:
            listen(wave2, f=f)

        return wave2
    except Exception as msg:
        lg.exception(msg)


################################
def main():
    try:

        # model = phyloglm()

        # df = pd.DataFrame(
        #     {"score": [77, 79, 84, 85, 88, 99, 95, 90, 92, 94], "hours": [1, 1, 2, 3, 2, 4, 4, 2, 3, 3], "prac_exams": [2, 3, 3, 2, 4, 5, 4, 3, 5, 4]}
        # )

        # lgr.debug(df)
        # fit = lm(score ~ hours + prac_exams, data = df)
        # lgr.debug(fit)
        # d = D_range(
        #     [5.0, 5.2, 5.9],
        #     [100, 110, 120],
        #     [[5000, 80], [5100, 70], [5200, 75], [5300, 105], [5400, 85], [5500, 65], [5600, 60], [5700, 105], [5800, 85], [5900, 95]],
        # )
        # lgr.debug(d)
        # read a recording
        rec_name = "./WAV/2023-01-17-001/MZ000011.WAV"
        w = Wave.read(rec_name, from_=2.0, to_=12.5)
        # w.spectro(palette="Greys", figsize=(10, 10), title="dipper", cmaplimits=(-20, 50))
        # oscillow(w)

        firLena(w, f=10000, from_=2, to_=155)
        # wx = w.cut(from_=1.0, to_=1.1)
        # wx.oscillo(title="before")
        # wx = wx.resample(newrate=22050)
        # wx.oscillo(title="after")
        # oscillow(w, fastdisp=True)
        exit(0)
        w3 = w.change_dB(-6)
        oscillow(w3)

        # build beeps
        w1 = beep(1000.0, 1.0)  # , plot=True, listen=True)
        w6 = beep(2000.0, 1.0)  # , plot=True, listen=True)
        w7 = Wave.concatenate([w1, w6])
        oscillow(w7)
        playw(w7)
        w8, w9 = w7.split(at=1.5)
        oscillow(w8, title="W7-1")
        oscillow(w9, title="w7-2")

        w12 = w.cut(from_=0.005, to_=0.015)
        w12.oscillo(title="cut2")
        w5 = Wave.noise(d=5, rate=44100)
        w5.oscillo()
        w5.play()

        # lgr.debug(w1.show(), w1.setValidity(), w1, title="w1")

        w10 = w1.addsil(at="middle", d=1, plot=True)  # , listen=True)
        oscillow(w10)
        w11 = w1.addsil(at="end", d=1.5, plot=True, listen=True)
        oscillow(w11, title="w11")
        playw(w11)
        w4 = make_calib_pb([1000, 1500, 3000], d=0.5, SNR=12)
        w4.oscillo(title="W4")
        playw(w4)
        w2 = w.cut(to_=0.01)
        oscillow(w2, title="cut1")

        # fade(w2, din=0.02, dout=0.02)  # , plot=True, listen=True)
        # lgr.debug(w2)

    except Exception as msg:
        lg.exception(msg)
        lgr.info("something went wrong: %s" % msg)
        exit(0)


#### Run main ##########################################################
if __name__ == "__main__":
    main()
