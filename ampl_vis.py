---
title: "dipper_ampl_vis"
author: "Lena"
date: "2023-03-08"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(ggplot2)
library(dplyr)
library(ggeffects)
library(lme4)

files = list.files("../call_data", full.names = TRUE, pattern = "*.csv")
list_files = lapply(1:length(files), function(x){
  x = read.csv(files[x])
  return(x)
})
dat = do.call(rbind, list_files)

dat$sp = "C.cinclus"
dat$sp = ifelse(dat$sess == "2023-02-09-001", "Henrik", dat$sp)
dat$cond = "spontaneous"
dat$cond = ifelse(dat$sess == "2023-02-28-001" & dat$recnb == "MZ000005", "Playback", dat$cond)

meta = read.csv("../WAV/Metadata (1).csv")
meta$unique = paste0(meta$Session, meta$Recording_nb)
dat$recnb = as.numeric(factor(dat$recnb, levels = unique(dat$recnb)))



dat$totfilt = sapply(1:length(dat$sp), function(i){ 
  max(dat$filt[i], dat$add_filt[i])
  })

ctr_red = function(x, reduce = TRUE){
  m = mean(x, na.rm = TRUE)
  if(reduce == TRUE){
    s = sd(x, na.rm = TRUE)
  }else{
    s = 1
  }
  res = (x-m)/s
  return(res)
}

```

```{r}

ggplot(dat, aes(x = rms_dB_SL_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density(alpha = .5, position = "stack")

ggplot(dat, aes(x = SNR_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density(alpha = .5, position = "stack")


ggplot(dat,aes(y = rms_dB_SL_6dB, x = SNR_6dB, col = Fp_kHz_6dB))+
  theme_classic()+  
  scale_color_viridis_c()+
  geom_point()

ggplot(dat,aes(y = rms_dB_SL_6dB, x = amb_dBA, col = SNR_6dB, size = cond))+
  theme_classic()+
  scale_color_viridis_c()+
  geom_point()

ggplot(dat,aes(y = rms_dB_SL_6dB, x = dist_bird_m, col = amb_dBA))+
  theme_classic()+
  scale_color_viridis_c()+
  geom_jitter(alpha = 0.2, width = 2, height = 2)


ggplot(dat, aes(x = call_dur_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_histogram(alpha = .5, position = "identity")

ggplot(dat, aes(x = ICI, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density( position = "stack")+
  xlim(0,1)

ggplot(dat, aes(x = Fp_kHz_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density(alpha = .5, position = "stack")

ggplot(dat, aes(x = loF_kHz_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density(alpha = .5, position = "stack")

ggplot(dat, aes(x = bw_6dB, fill = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  facet_wrap(~sp)+
  geom_density(alpha = .5, position = "stack")

datf = dat %>% 
  filter(SNR_6dB>3) %>%
  filter(sp == "C.cinclus")


ggplot(datf, aes(x = Fp_kHz_6dB, y = rms_dB_SL_6dB))+
  theme_classic()+
  theme(legend.position = "none")+
  geom_point(alpha = 0.5, aes(col = sess))+
  stat_smooth(method ="lm")


ggplot(datf, aes(x = loF_kHz_6dB, y = rms_dB_SL_6dB, col = sess))+
  theme_classic()+
  theme(legend.position = "none")+
  geom_point(alpha = 0.5, aes(col = sess))+
  stat_smooth(method ="lm")

ggplot(datf, aes(x = bw_6dB, y = rms_dB_SL_6dB))+
  theme_classic()+
  theme(legend.position = "none")+
  geom_point(alpha = 0.5, aes(col = sess))+
  stat_smooth(method ="lm")

ggplot(datf, aes(x = call_dur_6dB, y = rms_dB_SL_6dB))+
  theme_classic()+
  theme(legend.position = "none")+
  geom_point(alpha = 0.5, aes(col = sess))+
  stat_smooth(method ="lm")


datpb = dat %>% filter(sess == "2023-02-28-001")
ggplot(datpb, aes(x = rms_dB_SL_6dB, fill = cond))+
  theme_classic()+
  geom_density(alpha = .5, position = "identity")


datf2 = datf
datf2$rms_dB_SL_6dB = ctr_red(datf2$rms_dB_SL_6dB, reduce = F)
datf2$SNR_6dB = ctr_red(datf2$SNR_6dB, reduce = F)/6
datf2$dist_bird_m = ctr_red(datf2$dist_bird_m, reduce = F)/10
datf2$amb_dBA = ctr_red(datf2$amb_dBA, reduce = F)/6
datf2$totfilt = ctr_red(datf2$totfilt, reduce = F)/1000
datf2$cond = factor(datf2$cond, levels = c("spontaneous","Playback"))

# l = lmer(rms_dB_SL_6dB~SNR_6dB+amb_dBA+dist_bird_m+totfilt+cond+(1|sess), data = datf2)
summary(l)
confint(l)
plot(l)
qqnorm(resid(l));qqline(resid(l))
plot(ggpredict(l))
AIC(l)

```