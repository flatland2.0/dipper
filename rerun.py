#!/usr/bin/env python
# -*- coding: utf-8 -*-
__version__ = "1.0.0"
__author__ = "Léna de Framond/Jean de Framond"
__copyright__ = "Copyright 2021-2025 Léna de Framond"
__license__ = "MIT"
__description__ = "Analysis of dipper's songs"
__contact__ = "lena.de.framond@laposte.net"

import pandas as pd
import jflogging
import importlib

lgr = jflogging.Logger(name="Dipper")
lgr.set_debug(True)


file_name = paste0("../call_analysis/checked_",session, ".csv")
datafile = read.csv(file_name)

x=1
X11()
start = 0
new_res = lapply(1:length(datafile$exclude), function(x){
  if(datafile$exclude[x]==0){
    recname = paste0("../WAV/",datafile$sess[x], "/",datafile$recnb[x], ".WAV")
    rec = readWave(recname, from = datafile$starts_spl[x], to = datafile$ends_spl[x], units = "samples")
  
    HP = max(datafile$filt[x], datafile$add_filt[x])
  
    index_meta = which(meta$Session==datafile$sess[x] & meta$Recording_nb == datafile$recnb[x])
    TP = meta$Tp[index_meta]
    RH = meta$RH[index_meta]
    dist = meta$Dist_bird_m[index_meta]
  
    # correct for distance-dependent atmospheric attenuation and filter
    AA_frqs = seq(0,f/2, length.out = 256)
    AA_dBs = f_aa(AA_frqs, TP, RH)*dist
    AA_chup = ifelse(AA_frqs>HP & AA_frqs<15000,AA_dBs, -60)
    AA_filt = undB(AA_chup, ref = 1)
    rec_filt = firLena(rec, custom = AA_filt, output = "Wave")

  
    tmsp = data.frame(starts = 1,ends = length(rec_filt))
    call_params = analyse_calls(rec_filt, tmsp, dB_thr = ana_thr, start = start)
    print(ggspectro(rec_filt, ymax = 12))
  }
})
datafile = datafile %>% filter(exclude == 0)

all_call_pars = do.call(rbind, new_res)
addcols = c("recnb",	"sess",	"filt",	"starts_spl",	"ends_spl",	"exclude",	"add_filt")
all_call_pars = cbind(all_call_pars, datafile[,addcols])

i=1
calc_ampl = lapply(1:length(sess), function(i){ 
  
  call_params = all_call_pars[which(all_call_pars$recnb == meta$Recording_nb[sess[i]]),]
  
  # read calibration file (just first three seconds after calib started)
  calib_name = paste0("../WAV/",meta$Session[sess[i]], "/", meta$Calibration_file[sess[i]], ".WAV")
  calib = readWave(calib_name, from = meta$Sec_start_calib[sess[i]], to = meta$Sec_start_calib[sess[i]]+3, units = "seconds")
  par(mfrow=c(1,1))
  spectro(calib, fastdisp = TRUE)
  
  transfer = make_transfer(calib, f_tone = 5000, RL_dBA = meta$Ampl_calib_dBA[sess[i]], f=f)
  
  j=1
  names = lapply(1:length(ana_thr), function(j){
    varname = paste0("rms_dB_FS_", -1*ana_thr[j], "dB")
    newcol = paste0("rms_dB_RL_", -1*ana_thr[j], "dB")
    newcol2 = paste0("rms_dB_SL_", -1*ana_thr[j], "dB")
    varname3 = paste0("pe_dB_FS_", -1*ana_thr[j], "dB")
    newcol3 = paste0("pe_dB_RL_", -1*ana_thr[j], "dB")
    newcol4 = paste0("pe_dB_SL_", -1*ana_thr[j], "dB")
    name = data.frame(varname = rep(c(varname, varname3), each = 2), new = c(newcol, newcol2, newcol3, newcol4))
    return(name)
  })
  
  names = do.call(rbind, names)
  
  k=1
  ampls = lapply(seq(1,length(names$new),2), function(k){
    RL = call_params[,names$varname[k]] + transfer
    SL = call_params[,names$varname[k]] + transfer + dB(meta$Dist_bird_m[i], ref = 1)
    
    ret = data.frame(RL, SL)
    return(ret)
  })
  
  ampltab = do.call(cbind, ampls)
  colnames(ampltab) = names$new
  
  call_params = as.data.frame(cbind(call_params, ampltab))
  
  call_params$dist_bird_m = meta$Dist_bird_m[sess[i]]
  call_params$amb_dBA = meta$Ampl_noise_mean[sess[i]]
  
  call_params$ICI = NA
  for(k in 2:length(call_params$dB_thr_6dB)){
    call_params$ICI[k] = call_params$starts_spl[k]/f - call_params$ends_spl[k-1]/f
  }
  
  
  return(call_params)
})

fintab = do.call(rbind, calc_ampl)


col_order = c("sess", "recnb", "dist_bird_m", "amb_dBA", "filt", "add_filt", "call_time_6dB", "ICI", 
              paste0(c("call_dur", 
                       "Fp_kHz",
                       "loF_kHz",
                       "hiF_kHz",
                       "bw",
                       "Fstart_kHz",
                       "Fend_kHz",
                       "Fmax_ampl_kHz",
                       "pe_dB_FS",
                       "rms_dB_FS",
                       "pe_dB_RL",
                       "rms_dB_RL",
                       "pe_dB_SL",
                       "rms_dB_SL",
                       "SNR"), "_" ,rep(-1*ana_thr, each = 15),"dB" )
)
fintab = fintab[,col_order]

write.csv(fintab, paste0("../call_data/corrected_", session, ".csv"), row.names = F)
print("file saved! Your analysis is finished!")

